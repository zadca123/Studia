<?php
// Pokazuje podstronę poprzez wyszukany tytuł
function PokazPodstrone($id){
    global $conn;
    $id_clear = htmlspecialchars($id);
    $query="SELECT * FROM page_list WHERE page_title='$id_clear' LIMIT 1";
    // $query="SELECT * FROM page_list WHERE id='$id_clear' LIMIT 1";
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_array($result);

    //wywolywanie strony z bazy
    if (empty($row['id'])){
        $web = '[nie_znaleziono_strony]';
    }
    else{
        $web = $row['page_content'];
    }
    return $web;
}
?>

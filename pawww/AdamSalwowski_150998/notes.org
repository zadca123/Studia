#+TITLE: Notes and tips during exercises
use /mycli/ than /mysql/
* Linki do labów
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab1.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab2.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab3.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab4.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab5.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab6.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab7.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab8.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab9.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab10.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab11.pdf
https://enzomind.com/files/uwm/wyklady/ProjAppWeb/lab12.pdf
* Lab 6
** install mysql manually!!!
   https://dev.mysql.com/downloads/mysql/
** setup phpmyadmin
   follow this:
   https://www.linuxshelltips.com/install-phpmyadmin-in-linux/
   or this:
   https://www.linuxtechi.com/install-phpmyadmin-linux/
   if you enqounter error during passwordless login try this:
   https://phoenixnap.com/kb/access-denied-for-user-root-localhost
** creating:
*** database
    i dont remember...
*** table
    #+begin_src sql 
   CREATE TABLE page_list(
   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   page_title VARCHAR(255),
   page_content TEXT,
   status INT DEFAULT 1);
,
   PRIMARY KEY ( id )
);
    #+end_src
    
*** queries
    #+begin_src sql
      INSERT INTO  page_list(page_title, page_content) VALUES('faq','
      <h1>Środowiska graficzne systemu GNU/Linux</h1>
      Przed wyborem odpowiedniego dla własnych potrzeb środowiska graficznego staje kiedyś każdy użytkownik systemów Linux. Szczególnie dla początkujących użytkowników ich ilość może być przytłaczająca. Mam nadzieję, że uda mi się dziś choć trochę przybliżyć najbardziej znane środowiska graficzne, które możemy spotkać w większości dostępnych w internecie dystrybucji Linuksa.

      <h2>KDE</h2>

      Rozwój KDE rozpoczął się w 1996 roku, a jego pierwszą wersję wydano w 1998 roku. Jest to również, oprócz samego środowiska graficznego, zbiór dedykowanych aplikacji np.: Dolphin, Kdenlive, Okular, KMail, KOrganizer, Kate itd. Najnowsza wersja KDE nazywa się Plazma i występuje w dwóch odmianach – Plazma Desktop i Plazma Netbook. KDE to najbardziej konfigurowalne i elastyczne środowisko pulpitu dostępne na rynku. Wszystkich modyfikacji można dokonać z poziomu ustawień systemowych i nie potrzeba do tego dodatkowego oprogramowania czy pluginów. KDE jest oferowany przez wiele popularnych dystrybucji Linuksa, takich jak KDE neon, Fedora, Linux Mint, OpenSUSE, Kubuntu, PCLinuxOS. Jest także dostępny w wielu innych dystrybucjach.

      <h2>GNOME</h2>

      GNOME, który został wydany po raz pierwszy w 1999 roku, jest jednym z najpopularniejszych środowisk graficznych w świecie dystrybucji Linuksa. Wiele dystrybucji Linuksa używa go jako domyślnego środowiska dla pulpitu. GNOME został zaprojektowany tak, aby był łatwy w użyciu i konfigurowalny. Najnowsza wersja GNOME 3, która powstała w 2011 roku, ma nowoczesny i atrakcyjny interfejs użytkownika. Niestety zmiany dokonane w GNOME 2, który zapewniał prosty i klasyczny wygląd pulpitu, spotkały się z negatywnym odzewem wśród społeczności i programistów. Spowodowało to powstanie kilku alternatywnych środowisk jak Cinnamon i MATE. Z czasem jednak GNOME odzyskał swoją popularność i jest obecnie powszechnie wykorzystywany. Główne dystrybucje, które wykorzystują GNOME to Debian, Fedora, OpenSUSE i Ubuntu. Jest także dostępny w wielu innych dystrybucjach.

      <h2>MATE</h2>
      Projekt został stworzony przez społeczność systemu Arch Linux w 2011 roku. MATE swoją nazwę zawdzięcza pochodzącej z Ameryki Południowej roślinie yerba mate. Opiera się na bazie kodu obecnie już nierozwijanego GNOME 2 i był odpowiedzią na rozczarowanie użytkowników najnowszą odsłoną GNOME 3. Ponieważ jest oparty na środowisku graficznym, które od lat jest testowane i ulepszane, działa bezproblemowo. Obsługuje system paneli z różnymi menu, apletami, wskaźnikami, przyciskami itp. i może być dostosowany w dowolny sposób. MATE zawiera zestaw podstawowych aplikacji, z których większość to pochodne aplikacji z GNOME 2. Domyślnie są to między innymi: Caja (menedżer plików), Pluma (edytor tekstu), Atril (przeglądarka dokumentów), Eye of MATE (przeglądarka zdjęć). Zużywa on bardzo niewielką ilość pamięci na własne zasoby, dzięki czemu jest w stanie działać poprawnie na starszym i mniej wydajnym sprzęcie. Do dystrybucji, które używają tego środowiska graficznego, możemy zaliczyć między innymi Ubuntu MATE, Linux Mint, Mageia i Debian. Jest także dostępny w wielu innych dystrybucjach.');
      INSERT INTO  page_list(page_title, page_content) VALUES('filmy','
      <h1>Filmy nawiązujące do tematyki GNU\Linux?</h1>

      <h2>The Social Network</h2>
      Film ma niezbyt wiele z systemem operacyjnym GNU/Linux ale to jedyny pełnoprawny film o krórym mam pojęcie, że istnieje i posiada takie nawiązania.Na scenach, gdy widać ekrany laptopów czy komputerów możemy zauważyć że większość ma zainstalowany system UNIX z graficznym interfejsem KDE (obecnie wygląda inaczej, lecz istnieje tak zwany "fork" który zachowuje oldschoolowy wygląd). Na ujęciach widać użytkowanie z terminalu i komend. Obecny jest także edytor tekstowy Emacs. Można także wspomnieć, że Quantino Tarantino uznał ten film za najlepszy film 2010 roku.<br>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/lB95KLmpLR4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <br><br>
      <h2>The Code</h2>
      Historia powstania systemu operacyjnego GNU/Linux.<br>
      <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/XMm0HsmOTFI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <br><br>

      <h2>Coś o ruchu "free software" i "open source"</h2>
      <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Ag1AKIl_2GM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      ');
      INSERT INTO  page_list(page_title, page_content) VALUES('start','
      <h1>Podstawowe polecenia</h1>
      <h2>I. Polecenia związane z użytkownikami, grupami, loginami i zamykaniem systemu</h2>
      <ul>
	<li align="left">shutdown(zamykamy Linuxa)</li>
	<li align="left">adduser (dodajemy nowego użytkownika)</li>
	<li align="left">newgrp (dodajemy nową grupę)</li>
	<li align="left">passwd (zmieniamy hasła)</li>
	<li align="left">logout (wylogowanie się)</li>
	<li align="left">who (sprawdzamy kto jest aktualnie zalogowany)</li>
	<li align="left">users (j/w)</li>
	<li align="left">w (j/w)</li>
	<li align="left">whoami (sprawdzamy kim jesteśmy)</li>
	<li align="left">mesg (zezwolenie na przyjmowania komunikatów)</li>
	<li align="left">write (wysłanie wiadomości do danego użytkownika)</li>
	<li align="left">wall (j/w tylko do wszystkich użytkowników)</li>
	<li align="left">rwall (j/w tylko do wszystkich w sieci)</li>
	<li align="left">ruser (wyświetla użytkowników pracujących w systemie)</li>
	<li align="left">talk (możliwość interaktywnej rozmowy)</li>
	<li align="left">finger(szczegółowe informacje o użytkownikach)</li>
	<li align="left">su (zmieniamy się w innego użytkownika)</li>
	<li align="left">chmod (zmieniamy parametry pliku)</li>
	<li align="left">chown (zmieniamy właściciela pliku)</li>
	<li align="left">chgrp (zmieniamy jaka grupa jest właścicielem pliku)</li>
      </ul>

      <h2>Polecenia związane z procesami</h2>
      <ul>
	<li>ps (pokazuje nam jakie procesy są aktualnie wykonywane)</li>
	<li>kill ("zabijamy" procesy)</li>
      </ul>

      <h2>IV. Polecenia związane z pomocą</h2>
      <ul>
	<li>help (wyświetla nam wszystkie polecenia w Linuxie)</li>
	<li>man (pokazuje nam pomoc do programu)</li>
      </ul>

      <h2>V. Polecenia związane z kompresją i archiwilizacją</h2>
      <ul>
	<li>gzip(kompresuje nam archiwum *.gz)</li>
	<li>tar (archiwizuje nam archiwum *.tar)</li>
      </ul>

      <h1>Co dalej...</h1>
      <p>Po więcej informacji i opinii o różnych dystybucjach GNU\Linux zapraszam na
	<a href="https://www.distrowatch.com/">tą stronę.</a>
	<table>
	  <tr>
	    <th>Dystrybucje dla początkującego</th>
	    <th>Dystrybucje dla zaawansowanych</th>
	  </tr>
	  <tr>
	    <td>Ubuntu</td>
	    <td>Arch Linux</td>
	  </tr>
	  <tr>
	    <td>Linux Mint</td>
	    <td>Gentoo</td>
	  </tr>
	  <tr>
	    <td>Pop!OS</td>
	    <td>LinuxFromScrach</td>
	  </tr>
	</table>
      ');
      INSERT INTO  page_list(page_title, page_content) VALUES('kontakt','
      <h1><u>Wpisz swoje dane kontaktowe</u></h1>
      <h2><i>Formularz kontaktowy</i><br></h2>
      <form action="mailto:150998@student.uwm.edu.pl" method="post" enctype="text/plain">
	Adres e-mail:<br><input type="text" name="email"> <br>
	Imię:<br><input type="text" name="fname"><br>
	Nazwisko:<br><input type="text" name="lname"><br><br>
	<input style="font-size:110%" type="submit" value="Wyślij">
      </form>

      <br><br>
      <h1><u>Test PHP</u></h1>
      <a href="php/labor_150998_gr3.php?cos1=UŻYTKOWNIKU&cos2=NASZEJ_STRONIE">Test $GET</a>
      <form action="php/labor_150998_gr3.php" method="post">
	Imię:<br><input type="text" name="fname"><br>
	Nazwisko:<br><input type="text" name="lname"><br>
	Wiek:<br><input type="text" name="age"><br><br>
	<input style="font-size:110%" type="submit" value="Wyślij">
      </form>
      ');
      INSERT INTO  page_list(page_title, page_content) VALUES('galeria','
      <h2 id="title">Proszę kliknąć w interesujęce zdjęcie</h2>
      <b>Są to przykładowe zdjęcia / screenshoty głównego ekranu użytkowników różnych dystrybucji systemu GNU\Linux których hobby jest dopieszczanie UI, posiadanie spójnego motywu kolorystycznego, zestaw ich ulubionych aplikacji itd...</b><br></br>
      <img src="img/rice07.png" width="150" height="100">
      <img src="img/rice08.png" width="150" height="100">
      <img src="img/rice09.png" width="150" height="100">
      <img src="img/rice10.png" width="150" height="100">
      <img src="img/rice11.png" width="150" height="100">
      <img src="img/rice12.png" width="150" height="100">
      <img src="img/rice13.png" width="150" height="100">
      <img src="img/rice14.png" width="150" height="100">
      <img src="img/rice15.png" width="150" height="100">
      <img src="img/rice16.png" width="150" height="100">
      <img src="img/rice17.png" width="150" height="100">
      <img src="img/rice18.png" width="150" height="100">
      <img src="img/rice19.png" width="150" height="100">
      <img src="img/rice20.png" width="150" height="100">
      <img src="img/rice21.png" width="150" height="100">
      <img src="img/rice22.png" width="150" height="100">
      <img src="img/rice23.png" width="150" height="100">
      <img src="img/rice24.png" width="150" height="100">
      <img src="img/rice25.png" width="150" height="100">
      <script>
	// animacjaTestowa1
	$("img").on("click", function () {
	    $(this).animate({
		width: "100%",
		height: "100%",
		// opacity: 0.9,
		// fontSize: "3em",
		// borderWidth: "10px"
	    }, 1200); //speed of animation
	});
	// // // animacjaTestowa2
	// $("h2").on({
	//     "mouseover": function () {
	// 	$(this).animate({
	// 	    width: "75%",
	// 	    // height: "50%",
	// 	}, 500);
	//     },
	//     "mouseout": function () {
	// 	$(this).animate({
	// 	    width: "100%",
	// 	    // height: 200,
	// 	}, 500);
	//     }
	// });
	// // animacjaTestowa3
	// $("#animacjaTestowa3").on("click", function () {
	//     if (!$(this).is(":animated")) {
	// 	$(this).animate({
	// 	    width: "+=" + "10%",
	// 	    height: "+=" + "10%",
	// 	    // height: "+=" + 10,
	// 	    // opacity: "-=" + 0.1,
	// 	    duration: 3000 //inny sposób deklaracji czasu trwania animacji
	// 	});
	//     }
	// });
      </script>
      ');
      INSERT INTO  page_list(page_title, page_content) VALUES('blog','
	      <h1>Popatrz na tego śmiesznego pingwinka</h1>
	      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet aliquam id diam maecenas ultricies. Massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Amet nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Mattis rhoncus urna neque viverra justo nec ultrices. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Tristique magna sit amet purus gravida quis blandit turpis cursus. Sed libero enim sed faucibus turpis. Nunc mi ipsum faucibus vitae. Tincidunt augue interdum velit euismod in pellentesque massa placerat. Nisi porta lorem mollis aliquam. Risus quis varius quam quisque id diam vel quam elementum. Dui vivamus arcu felis bibendum ut tristique. Sed id semper risus in. Donec et odio pellentesque diam volutpat. Vitae et leo duis ut diam quam. Volutpat odio facilisis mauris sit amet massa vitae. Ut faucibus pulvinar elementum integer enim.<br></br>
	      <img align="left" src="img/tux-got-root.gif">
	      Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Dignissim diam quis enim lobortis. Mollis aliquam ut porttitor leo. Ut tortor pretium viverra suspendisse potenti nullam ac tortor. Sem nulla pharetra diam sit amet nisl suscipit adipiscing. Orci a scelerisque purus semper eget. Morbi tincidunt augue interdum velit euismod in pellentesque. Nunc mi ipsum faucibus vitae. Augue interdum velit euismod in pellentesque massa placerat duis ultricies. Nunc mattis enim ut tellus elementum sagittis vitae. Proin sed libero enim sed faucibus. Tristique risus nec feugiat in fermentum posuere urna nec tincidunt.<br></br>
	      <img align="right" src="img/tuxman.gif">
	      Bibendum enim facilisis gravida neque. Posuere urna nec tincidunt praesent semper feugiat nibh sed. Non enim praesent elementum facilisis. Massa placerat duis ultricies lacus sed turpis tincidunt id. Amet porttitor eget dolor morbi non arcu. Consectetur adipiscing elit ut aliquam purus sit. Quisque sagittis purus sit amet volutpat. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium. Nulla porttitor massa id neque aliquam vestibulum morbi blandit cursus. Eleifend mi in nulla posuere sollicitudin aliquam. Lorem mollis aliquam ut porttitor leo a diam sollicitudin. Nunc aliquet bibendum enim facilisis gravida neque. Praesent elementum facilisis leo vel. Ante in nibh mauris cursus mattis molestie. Lobortis mattis aliquam faucibus purus in. Dignissim diam quis enim lobortis scelerisque fermentum dui. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Ac turpis egestas maecenas pharetra.<br></br>

	      Vestibulum lorem sed risus ultricies tristique nulla aliquet enim tortor. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nunc aliquet bibendum enim facilisis gravida neque convallis a. Eleifend quam adipiscing vitae proin. Amet purus gravida quis blandit turpis. Malesuada bibendum arcu vitae elementum curabitur vitae nunc. Purus gravida quis blandit turpis cursus. Erat velit scelerisque in dictum non consectetur a. Et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Amet massa vitae tortor condimentum. Amet luctus venenatis lectus magna fringilla urna. Facilisis magna etiam tempor orci eu lobortis elementum. Ante metus dictum at tempor commodo ullamcorper. Lobortis mattis aliquam faucibus purus in massa tempor nec. Tortor condimentum lacinia quis vel.<br></br>
	      Gravida neque convallis a cras semper auctor neque vitae tempus. Donec pretium vulputate sapien nec sagittis aliquam malesuada. At varius vel pharetra vel. Enim lobortis scelerisque fermentum dui faucibus. Purus gravida quis blandit turpis cursus. At consectetur lorem donec massa. In nulla posuere sollicitudin aliquam ultrices sagittis orci a. Commodo odio aenean sed adipiscing diam donec adipiscing tristique. Nibh tellus molestie nunc non blandit massa enim nec dui. Ac turpis egestas maecenas pharetra convallis posuere morbi leo.<br></br>');
      INSERT INTO  page_list(page_title, page_content) VALUES('glowna','
      <h2 id="title">Reject Windows, return to GNU\Linox</h2><br>
      <img width="75%" src="img/stallman.png"/>
      ');
    #+end_src
* Lab 10
** mysql
*** tabela
    #+begin_src sql 
   CREATE TABLE kategorie(
   id INT NOT NULL AUTO_INCREMENT,
   matka INT DEFAULT 0,
   nazwa VARCHAR(255),
   PRIMARY KEY (id));
    #+end_src
*** wartości
    #+begin_src sql 
   INSERT INTO kategorie(id, nazwa) VALUES(1, 'pluszak');
   INSERT INTO kategorie(id, nazwa) VALUES(2, 'brelok');
   INSERT INTO kategorie(id, nazwa) VALUES(3, 'zwierze');
   INSERT INTO kategorie(matka, nazwa) VALUES(1, 'brzydki');
   INSERT INTO kategorie(matka, nazwa) VALUES(1, 'ladny');
   INSERT INTO kategorie(matka, nazwa) VALUES(2, 'metalowy');
   INSERT INTO kategorie(matka, nazwa) VALUES(2, 'plastikowy');
   INSERT INTO kategorie(matka, nazwa) VALUES(3, 'duze');
   INSERT INTO kategorie(matka, nazwa) VALUES(3, 'male');
    #+end_src
* Lab 11
** mysql
*** tabela
    #+begin_src sql 
      CREATE TABLE produkty(
      id INT NOT NULL AUTO_INCREMENT,
      tytul VARCHAR(255) DEFAULT 'TYTUL PRODUKTU PLACEHOLDER',
      opis TEXT,
      data_utworzenia DATE DEFAULT(CURRENT_DATE()),
      data_modyfikacji DATE DEFAULT(CURRENT_DATE()),
      data_wygasniecia DATE DEFAULT(CURRENT_DATE() + INTERVAL 1 YEAR),
      cena_netto DECIMAL(10,2) NOT NULL DEFAULT 100,
      podatek_vat INT(2) DEFAULT 23,
      ilosc INT DEFAULT 1,
      status INT DEFAULT 1,
      kategoria INT DEFAULT 1,
      gabaryt CHAR DEFAULT 'A',
      -- zdjecie BLOB,
      zdjecie VARCHAR(255) DEFAULT 'placeholder.png',
      PRIMARY KEY (id));
    #+end_src
*** wartości
    #+begin_src sql
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z madagaskaru', 669,  8,  'produkt_zwierze1.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z madagaskaru', 2342, 9,  'produkt_zwierze2.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z madagaskaru', 455,  8,  'produkt_zwierze3.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z madagaskaru', 355,  8,  'produkt_zwierze4.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z antarktydy',  669,  9,  'produkt_zwierze5.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z antarktydy',  228,  6,  'produkt_brelok1.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z antarktydy',  443,  7,  'produkt_brelok2.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z antarktydy',  243,  7,  'produkt_brelok3.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z urugwaju',    9,    4,  'produkt_pluszak1.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z urugwaju',    3434, 5,  'produkt_pluszak2.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z urugwaju',    6,    4,  'produkt_pluszak3.jpeg');
      INSERT INTO produkty(tytul, opis, cena_netto, kategoria, zdjecie) VALUES('pingwin', 'z urugwaju',    2,    5,  'produkt_pluszak4.jpeg');
    #+end_src
* Lab 12
  #+begin_src sql
      CREATE TABLE koszyk(
      id INT NOT NULL AUTO_INCREMENT,
      tytul VARCHAR(255) DEFAULT 'TYTUL PRODUKTU PLACEHOLDER',
      data_utworzenia DATE DEFAULT(CURRENT_DATE()),
      data_modyfikacji DATE DEFAULT(CURRENT_DATE()),
      data_wygasniecia DATE DEFAULT(CURRENT_DATE() + INTERVAL 1 YEAR),
      cena_netto DECIMAL(10,2) NOT NULL DEFAULT 100,
      podatek_vat INT(2) DEFAULT 23,
      ilosc INT DEFAULT 1,
      -- zdjecie BLOB,
      zdjecie VARCHAR(255) DEFAULT 'placeholder.png',
      PRIMARY KEY (id));
  #+end_src

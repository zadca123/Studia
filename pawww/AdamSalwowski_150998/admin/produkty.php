<?php
session_start();
include("../cfg.php");

class Produkty{
    private $id,$tytul,$opis,$data_wygasniecia,$cena_netto,$podatek_vat,$ilosc,$status,$kategoria,$gabaryt,$zdjecie,$produkty_arr;

    public function __construct($id = NULL){
        if ($id != NULL){
            include("../cfg.php");
            $result = mysqli_query($conn, "SELECT * FROM produkty WHERE id=$id");
            $produkt = $result->fetch_object();
            $this->id              = $produkt->id;
            $this->tytul           = $produkt->tytul;
            $this->opis            = $produkt->opis;
            $this->data_wygasniecia= $produkt->data_wygasniecia;
            $this->cena_netto      = $produkt->cena_netto;
            $this->podatek_vat     = $produkt->podatek_vat;
            $this->ilosc           = $produkt->ilosc;
            $this->status          = $produkt->status;
            $this->kategoria       = $produkt->kategoria;
            $this->gabaryt         = $produkt->gabaryt;
            $this->zdjecie         = $produkt->zdjecie;
            // $this->produkty_arr = mysqli_fetch_all($result, MYSQLI_ASSOC);
            mysqli_free_result($result);
            mysqli_close($conn);
        }else{
            $this->id              = 0;
            $this->tytul           = 'tytul_produktu placeholder';
            $this->opis            = 'opis';
            $this->data_wygasniecia= '2022-12-12';
            $this->cena_netto      = 100;
            $this->podatek_vat     = 23;
            $this->ilosc           = 1;
            $this->status          = 1;
            $this->kategoria       = 1;
            $this->gabaryt         = 'A';
            $this->zdjecie         = 'placeholder.png';

        }
    }

    function setArray(){
        include("../cfg.php");
        $result = $conn->query("SELECT * FROM produkty");
        // $result = mysqli_query($conn, "SELECT * FROM produkty");
        $this->produkty_arr = mysqli_fetch_all($result, MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
    }
    // gettery
    function getId(){
        return $this->id;
    }
    function getTytul(){
        return $this->tytul;
    }
    function getOpis(){
        return $this->opis;
    }
    function getData_Modyfikacji(){
        return $this->data_modyfikacji;
    }
    function getData_Wygasniecia(){
        return $this->data_wygasniecia;
    }
    function getCena_Netto(){
        return $this->cena_netto;
    }
    function getPodatek_Vat(){
        return $this->podatek_vat;
    }
    function getIlosc(){
        return $this->ilosc;
    }
    function getStatus(){
        return $this->status;
    }
    function getKategoria(){
        return $this->kategoria;
    }
    function getGabaryt(){
        return $this->gabaryt;
    }
    function getZdjecie(){
        return $this->zdjecie;
    }

    // settery
    function setId($id){
        $this->id = $id;
    }
    function setTytul($tytul){
        $this->tytul = $tytul;
    }
    function setOpis($opis){
        $this->opis = $opis;
    }
    function setData_Modyfikacji($data_modyfikacji){
        $this->data_modyfikacji = $data_modyfikacji;
    }
    function setData_Wygasniecia($data_wygasniecia){
        $this->data_wygasniecia = $data_wygasniecia;
    }
    function setCena_Netto($cena_netto){
        $this->cena_netto = $cena_netto;
    }
    function setPodatek_Vat($podatek_vat){
        $this->podatek_vat = $podatek_vat;
    }
    function setIlosc($ilosc){
        $this->ilosc = $ilosc;
    }
    function setStatus($status){
        $this->status = $status;
    }
    function setKategoria($kategoria){
        $this->kategoria = $kategoria;
    }
    function setGabaryt($gabaryt){
        $this->gabaryt = $gabaryt;
    }
    function setZdjecie($zdjecie){
        $this->zdjecie = $zdjecie;
    }
}

if(isset($_GET['usun'])){
    $produkt = new Produkty(mysqli_real_escape_string($conn, $_GET['usun']));
    mysqli_query($conn, 'usun FROM produkty WHERE id=' . $produkt->getId());
}
if(isset($_GET['edytuj'])){
    $produkt = new Produkty(mysqli_real_escape_string($conn, $_GET['edytuj']));
    echo '<h2>Edytuj produkt</h2>';
    echo Formularz($produkt);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $produkt->setTytul($_POST['tytul']);
        $produkt->setOpis($_POST['opis']);
        $produkt->setData_Wygasniecia($_POST['data_wygasniecia']);
        $produkt->setCena_Netto($_POST['cena_netto']);
        $produkt->setPodatek_Vat($_POST['podatek_vat']);
        $produkt->setIlosc($_POST['ilosc']);
        $produkt->setStatus($_POST['status']);
        $produkt->setKategoria($_POST['kategoria']);
        $produkt->setGabaryt($_POST['gabaryt']);
        $produkt->setZdjecie($_POST['zdjecie']);
        mysqli_query($conn, "UPDATE produkty SET tytul='" . $produkt->getTytul() . "',opis='" . $produkt->getOpis() ."', data_wygasniecia='" . $produkt->getData_Wygasniecia() . "', cena_netto='" . $produkt->getCena_Netto() . "', podatek_vat='" . $produkt->getPodatek_Vat() . "', ilosc='" . $produkt->getIlosc() . "', status='" . $produkt->getStatus() . "', kategoria='" . $produkt->getKategoria() . "', gabaryt='" . $produkt->getGabaryt() . "', zdjecie='" . $produkt->getZdjecie() . "' WHERE id='" . $produkt->getId() . "'");
    }
}
if(isset($_GET['dodaj'])){
    $produkt = new Produkty();
    echo '<h2>Dodaj produkt</h2>';
    echo Formularz($produkt);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $produkt->setTytul($_POST['tytul']);
        $produkt->setOpis($_POST['opis']);
        $produkt->setData_Wygasniecia($_POST['data_wygasniecia']);
        $produkt->setCena_Netto($_POST['cena_netto']);
        $produkt->setPodatek_Vat($_POST['podatek_vat']);
        $produkt->setIlosc($_POST['ilosc']);
        $produkt->setStatus($_POST['status']);
        $produkt->setKategoria($_POST['kategoria']);
        $produkt->setGabaryt($_POST['gabaryt']);
        $produkt->setZdjecie($_POST['zdjecie']);
        mysqli_query($conn, "INSERT INTO produkty(tytul, opis, data_wygasniecia, cena_netto, podatek_vat, ilosc, status, kategoria, gabaryt, zdjecie) VALUES('" . $produkt->getTytul() . "', '" . $produkt->getOpis() . "','" . $produkt->getData_Wygasniecia() . "','" . $produkt->getCena_Netto() . "','" . $produkt->getPodatek_Vat() . "','" . $produkt->getIlosc() . "','" . $produkt->getStatus() . "','" . $produkt->getKategoria() . "','" . $produkt->getGabaryt() . "','" . $produkt->getZdjecie() . "')");
    }
}

function Formularz($produkt){
    echo '<form method="post">
    <table>
      <tr>
        <th>Id</th>
        <th>Tytuł</th>
        <th>Opis</th>
        <th>Data wygaśniecia</th>
        <th>Cena netto</th>
        <th>Podatek vat</th>
        <th>Ilość</th>
        <th>Status</th>
        <th>Kategoria</th>
        <th>Gabaryt</th>
        <th>Zdjęcie</th>
      </tr>
      <tr>
        <td><input type="number" disabled name="id" value=' . $produkt->getId() . '></td>
        <td><input type="text" name="tytul" value=' . $produkt->getTytul() . '></td>
        <td><textarea type="text" name="opis" value=' . $produkt->getOpis() . '></textarea></td>
        <td><input type="date" name="data_wygasniecia" value=' . $produkt->getData_Wygasniecia() . '></td>
        <td><input type="number" step=".01" name="cena netto" value=' . $produkt->getCena_Netto() . '></td>
        <td><input type="number" name="podatek_vat" value=' . $produkt->getPodatek_Vat() . '></td>
        <td><input type="number" name="ilosc" value=' . $produkt->getIlosc() . '></td>
        <td><input type="number" name="status" value=' . $produkt->getStatus() . '></td>
        <td><input type="number" name="kategoria" value=' . $produkt->getKategoria() . '></td>
        <td><input type="char" name="gabaryt" value=' . $produkt->getGabaryt() . '></td>
        <td><input type="text" name="zdjecie" value=' . $produkt->getZdjecie() . '></td>
      </tr>
    </table>
  <button type="submit" name="save">Zapisz</button>
    </form>';
}


?>

    <!DOCTYPE html>
        <html lang="en">
    <head>
    <meta charset="utf-8">
    <link   href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
    <h1>Panel CMS</h1>
    <a href="verify.php">Wyloguj</a><br>
    <h2>Pokaz produkty</h2>
    <table>
    <tr>
    <th>id</th>
    <th>tytul</th>
    <th>opis</th>
    <th>data_utworzenia</th>
    <th>data_modyfikacji</th>
    <th>data_wygasniecia</th>
    <th>cena_netto</th>
    <th>podatek_vat</th>
    <th>ilosc</th>
    <th>status</th>
    <th>kategoria</th>
    <th>gabaryt</th>
    <th>zdjecie</th>
    </tr>
<?php
    $result = mysqli_query($conn, "SELECT * FROM produkty");
if ($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        ?>
        <tr>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['tytul']; ?></td>
        <td><?php echo $row['opis']; ?></td>
        <td><?php echo $row['data_utworzenia']; ?></td>
        <td><?php echo $row['data_modyfikacji']; ?></td>
        <td><?php echo $row['data_wygasniecia']; ?></td>
        <td><?php echo $row['cena_netto']; ?></td>
        <td><?php echo $row['podatek_vat']; ?></td>
        <td><?php echo $row['ilosc']; ?></td>
        <td><?php echo $row['status']; ?></td>
        <td><?php echo $row['kategoria']; ?></td>
        <td><?php echo $row['gabaryt']; ?></td>
        <td><img width=50 height=50 src="../img/<?php echo $row['zdjecie']; ?>"/></td>
        <td><a href="?edytuj=<?php echo $row['id']; ?>">Edytuj produkt</a></td>
        <td><a href="?usun=<?php echo $row['id']; ?>">Usuń produkt</a></td>
        </tr>
<?php } ?>
        </table>
        <a href="?dodaj=0">Dodaj produkt</a>
<?php } else { echo '0 results';} $conn->close(); ?>
    </div>
        </body>
        </html>

<?php
session_start();
include("../cfg.php");

class Podstrony{
    private $id,$tytul,$content,$status;

    public function __construct($id = NULL){
        if ($id != NULL){
            include("../cfg.php");
            $result = mysqli_query($conn, "SELECT * FROM page_list WHERE id=$id");
            $podstrona = $result->fetch_object();
            $this->id              = $podstrona->id;
            $this->tytul           = $podstrona->page_title;
            $this->content         = $podstrona->page_content;
            $this->status          = $podstrona->status;
            mysqli_free_result($result);
            mysqli_close($conn);
        }else{
            $this->id              = 0;
            $this->tytul           = 'tytul_placeholder';
            $this->content         = 'content_placeholder';
            $this->status          = 1;
        }
    }

    // gettery
    function getId(){
        return $this->id;
    }
    function getTytul(){
        return $this->tytul;
    }
    function getContent(){
        return $this->content;
    }
    function getStatus(){
        return $this->status;
    }

    // settery
    function setId($id){
        $this->id = $id;
    }
    function setTytul($tytul){
        $this->tytul = $tytul;
    }
    function setContent($content){
        $this->opis = $content;
    }
    function setStatus($status){
        $this->status = $status;
    }
}

if(isset($_GET['usun'])){
    $podstrona = new Podstrony(mysqli_real_escape_string($conn, $_GET['usun']));
    mysqli_query($conn, 'usun FROM page_list WHERE id=' . $podstrona->getId());
}
if(isset($_GET['edytuj'])){
    $podstrona = new Podstrony(mysqli_real_escape_string($conn, $_GET['edytuj']));
    echo '<h2>Edytuj podstrony</h2>';
    FormularzPodstrony($podstrona);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $podstrona->setTytul($_POST['tytul']);
        $podstrona->setContent($_POST['content']);
        $podstrona->setStatus($_POST['status']);
        mysqli_query($conn, "UPDATE page_list SET page_title='" . $podstrona->getTytul() . "',page_content='" . $podstrona->getContent() . "',status='" . $podstrona->getStatus() . "' WHERE id='" . $podstrona->getId() . "'");
    }
}
if(isset($_GET['dodaj'])){
    $podstrona = new Podstrony();
    echo '<h2>Dodaj podstrony</h2>';
    FormularzPodstrony($podstrona);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $podstrona->setTytul($_POST['tytul']);
        $podstrona->setContent($_POST['content']);
        $podstrona->setStatus($_POST['status']);
        mysqli_query($conn, "INSERT INTO page_list(page_title,page_content,status) VALUES('" . $podstrona->getTytul() . "', '" . $podstrona->getContent() . "','" . $podstrona->getStatus() . "');");
    }
}

function FormularzPodstrony($podstrona){
    echo $podstrona->getId();
    echo '<form method="post">
        <table>
        <tr>
        <th>Id</th>
        <th>Tytuł</th>
        </tr>
        <tr>
        <td><input    type="number" disabled   id="id"               name="id"      value=' . $podstrona->getId()               . '></td>
        <td><input    type="text"              id="tytul"            name="tytul"   value=' . $podstrona->getTytul()            . '></td>
        <td><textarea type="text"              id="content"          name="content" value=' . $podstrona->getContent()            . '></textarea></td>
        <td><input    type="ckeckbox"          id="status"           name="status"  value=' . $podstrona->getStatus()            . '></td>
        </tr>
        </table>
        <button type="submit" name="save">Zapisz</button>
        </form>';
}


?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <link   href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
    <h1>Panel CMS</h1>
    <a href="verify.php">Wyloguj</a><br>
    <h2>Pokaz podstrony</h2>
    <table>
    <tr>
    <th>id</th>
    <th>tytul</th>
    </tr>
<?php
$result = mysqli_query($conn, "SELECT * FROM page_list");
if ($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
?>
        <tr>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['page_title']; ?></td>
        <td><a href="?edytuj=<?php echo $row['id']; ?>">Edytuj podstronę</a></td>
        <td><a href="?usun=<?php echo $row['id']; ?>">Usuń podstronę</a></td>
        </tr>
<?php } ?>
        </table>
        <a href="?dodaj=0">Dodaj podstronę</a>
<?php } else { echo '0 results';} $conn->close(); ?>
    </div>
        </body>
        </html>

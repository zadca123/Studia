<?php
session_start();
include("../cfg.php");

class Kategorie{
    private $id,$matka,$nazwa;
    public function __construct($id = NULL){
        if ($id != NULL){
            include("../cfg.php");
            $result = mysqli_query($conn, "SELECT * FROM kategorie WHERE id=$id");
            $kategoria = $result->fetch_object();
            $this->id = $kategoria->id;
            $this->matka = $kategoria->matka;
            $this->nazwa = $kategoria->nazwa;
            mysqli_free_result($result);
            mysqli_close($conn);
        }else{
            $this->id = 0;
            $this->matka = 1;
            $this->nazwa = 'nazwa_placeholder';
        }
    }

    // gettery
    function getId(){
        return $this->id;
    }
    function getMatka(){
        return $this->matka;
    }
    function getNazwa(){
        return $this->nazwa;
    }

    // settery
    function setId($id){
        $this->id = $id;
    }
    function setMatka($matka){
        $this->opis = $matka;
    }
    function setNazwa($nazwa){
        $this->nazwa = $nazwa;
    }
}

if(isset($_GET['usun'])){
    $kategoria = new Kategorie(mysqli_real_escape_string($conn, $_GET['usun']));
    mysqli_query($conn, 'usun FROM kategorie WHERE id=' . $kategoria->getId());
}
if(isset($_GET['edytuj'])){
    $kategoria = new Kategorie(mysqli_real_escape_string($conn, $_GET['edytuj']));
    echo '<h2>Edytuj kategorie</h2>';
    FormularzKategorii($kategoria);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $kategoria->setNazwa($_POST['nazwa']);
        $kategoria->setMatka($_POST['matka']);
        mysqli_query($conn, "UPDATE kategorie SET matka='" . $kategoria->getMatka() . "',nazwa='" . $kategoria->getNazwa() . "' WHERE id='" . $kategoria->getId() . "'");
    }
}
if(isset($_GET['dodaj'])){
    $kategoria = new Kategorie();
    echo '<h2>Dodaj kategorie</h2>';
    FormularzKategorii($kategoria);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $kategoria->setMatka($_POST['matka']);
        $kategoria->setNazwa($_POST['nazwa']);
        mysqli_query($conn, "INSERT INTO kategorie(matka, nazwa) VALUES('" . $kategoria->getMatka() . "', '" . $kategoria->getNazwa() . "');");
    }
}

function FormularzKategorii($kategoria){
    echo '<form method="post">
        <table>
        <tr>
        <th>Id</th>
        <th>Matka</th>
        <th>Nazwa</th>
        </tr>
        <tr>
        <td><input type="number" disabled  id="id" name="id" value=' . $kategoria->getId() . '></td>
        <td><input type="number" id="matka" name="matka" value=' . $kategoria->getMatka() . '></td>
        <td><input type="text" id="nazwa" name="nazwa" value=' . $kategoria->getNazwa() . '></td>
        </tr>
        </table>
        <button type="submit" name="save">Zapisz</button>
        </form>';
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <link   href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
    <h1>Panel CMS</h1>
    <a href="verify.php">Wyloguj</a><br>
    <h2>Pokaz kategorie</h2>
    <table>
    <tr>
    <th>id</th>
    <th>matka</th>
    <th>nazwa</th>
    </tr>
<?php
$result = mysqli_query($conn, "SELECT * FROM kategorie");
if ($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
?>
        <tr>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['matka']; ?></td>
        <td><?php echo $row['nazwa']; ?></td>
        <td><a href="?edytuj=<?php echo $row['id']; ?>">Edytuj kategorie</a></td>
        <td><a href="?usun=<?php echo $row['id']; ?>">Usuń kategorie</a></td>
        </tr>
<?php } ?>
        </table>
        <a href="?dodaj=0">Dodaj kategorie</a>
<?php } else { echo '0 results';} $conn->close(); ?>
    </div>
        </body>
        </html>

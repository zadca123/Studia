<!DOCTYPE html>
    <html lang="en">
     <head>
     <meta charset="utf-8">
     <link   href="../css/bootstrap.min.css" rel="stylesheet">
     <script src="../js/bootstrap.min.js"></script>
     </head>
     <body>
     <div class="container">
     <h1>Panel CMS</h1>
     <a href="verify.php">Wyloguj</a><br>
<?php
     function ListaPodstron()
     {
         include("../cfg.php");
         $result = $conn->query("SELECT * FROM kategorie");
         echo "<h2>Pokaz produkty</h2>";
         echo "<table>";
         echo "<tr>";
         echo "<th>id</th>";
         echo "<th>tytul</th>";
         echo "</tr>";
         if ($result->num_rows > 0) {
             while ($row = $result->fetch_assoc()) {
                 echo "<tr>";
                 echo "<td>" . $row["id"] . "</td>";
                 echo "<td>" . $row["page_title"] . "</td>";
                 echo "<td><a href='?edit=" . $row['id'] . "'>EDYTUJ</a></td>";
                 echo "<td><a href='?delete=" . $row['id'] . "'>USUŃ</a></td>";
                 echo "</tr>";
             }
             echo "</table>";
             echo "<a href='?add=0'>DODAJ</a>";
         } else {
             echo "0 results";
         }
         $conn->close();
     }

     // Pozwala dodać nową podstronę
     if (isset($_GET['add'])) {
         include("../cfg.php");
         $id = $_GET['add'];
         $update = true;
         echo "<form method='post'>
            Indeks<br>   <input type='number' name='id' id='id' disabled><br><br>
            Tytuł:<br>   <input type='text' name='page_title' id='page_title'><br><br>
            Zawartość<br><textarea type='text' name='page_content' id='page_content'></textarea><br><br>
            Status<br>   <input type='checkbox' checked='checked' name='status' id='status' value=1>Aktywna<br><br>
            <button type='submit' name='save' >Zapisz</button><br><br>
          </form>";
         if ($_SERVER['REQUEST_METHOD'] === 'POST') {
             $page_title = $_POST['page_title'];
             $page_content = $_POST['page_content'];
             $status = $_POST['status'];
             mysqli_query($conn, "INSERT INTO page_list (page_title, page_content, status) VALUES ( '$page_title', '$page_content', '$status'); ");
         }
     }

// Pozwala edytować podstonę
if (isset($_GET['edit'])) {
    include("../cfg.php");
    $id = $_GET['edit'];
    $update = true;
    $record = mysqli_query($conn, "SELECT * FROM page_list WHERE id=$id");
    foreach ($record as $row) {
        echo "<form method='post'>
                Indeks<br>    <input type='number' name='id' id='id' disabled value=" . $row['id'] . "><br><br>
                Tytuł<br>     <input type='text' name='page_title' id='page_title' value=" . $row['page_title'] . "><br><br>
                Zawartość<br> <textarea type='text' name='page_content' id='page_content' value=" . $row['page_content'] . "></textarea><br><br>
                Status<br>    <input type='checkbox' checked='checked' name='status' id='status' value=1>Aktywna<br><br>
                <button type='submit' name='save' >Zapisz</button><br><br>
             </form>";
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $page_title = $_POST['page_title'];
            $page_content = $_POST['page_content'];
            $status = $_POST['status'];
            mysqli_query($conn, "UPDATE page_list SET page_title='$page_title', page_content='$page_content', status='$status' WHERE id=$id");
        }


    }
}

// Pozwala usuwać podstonę
if (isset($_GET['delete'])) {
    include("../cfg.php");
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM page_list WHERE id=$id");
}

// Wyświetla podstrony
ListaPodstron();
?>
        </div>
            </body>
            </html>

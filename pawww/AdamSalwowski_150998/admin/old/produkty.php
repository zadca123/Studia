<!DOCTYPE html>
    <html lang="en">
     <head>
     <meta charset="utf-8">
     <link   href="../css/bootstrap.min.css" rel="stylesheet">
     <script src="../js/bootstrap.min.js"></script>
     </head>
     <body>
     <div class="container">
     <h1>Panel CMS</h1>
     <a href="verify.php">Wyloguj</a><br>

<?php
     function PokazProdukty(){
         include("../cfg.php");
         $result = $conn->query("SELECT * FROM produkty");
                 echo '<h2>Pokaz produkty</h2>
                     <table>
                     <tr>
                     <th>id</th>
                     <th>tytul</th>
                     <th>opis</th>
                     <th>data_utworzenia</th>
                     <th>data_modyfikacji</th>
                      <th>data_wygasniecia</th>
                      <th>cena_netto</th>
                      <th>podatek_vat</th>
                      <th>ilosc</th>
                      <th>status</th>
                      <th>kategoria</th>
                      <th>gabaryt</th>
                      <th>zdjecie</th>
                      </tr>';
                      if ($result->num_rows > 0) {
                          while ($row = $result->fetch_assoc()) {
                              echo "<tr>";
                              echo "<td>" . $row["id"] . "</td>";
                              echo "<td>" . $row["tytul"] . "</td>";
                              echo "<td>" . $row["opis"] . "</td>";
                              echo "<td>" . $row["data_utworzenia"] . "</td>";
                              echo "<td>" . $row["data_modyfikacji"] . "</td>";
                              echo "<td>" . $row["data_wygasniecia"] . "</td>";
                              echo "<td>" . $row["cena_netto"] . "</td>";
                              echo "<td>" . $row["podatek_vat"] . "</td>";
                              echo "<td>" . $row["ilosc"] . "</td>";
                              echo "<td>" . $row["status"] . "</td>";
                              echo "<td>" . $row["kategoria"] . "</td>";
                              echo "<td>" . $row["gabaryt"] . "</td>";
                              echo "<td><img width=50 height=50 src='../img/" . $row["zdjecie"] . "'/></td>";
                              // echo "<td><img src='data:image/jpeg;base64,".base64_encode( $row['zdjecie'] )."'/></td>";
                              echo "<td><a href='?edit=" . $row['id'] . "'>EDYTUJ</a></td>";
                              echo "<td><a href='?delete=" . $row['id'] . "'>USUŃ</a></td>";
                              echo "</tr>";
                          }
                          echo "</table>";
                          echo "<a href='?add=0'>DODAJ</a>";
                      } else {
                          echo "0 results";
                      }
                      $conn->close();
     }

     function FormularzPodstrony($id,$tytul,$opis,$data_wygasniecia,$cena_netto,$podatek_vat,$ilosc,$status,$kategoria,$gabaryt,$zdjecie){
         /* function FormularzPodstrony(){ */
         echo "<form method='post'>";
         echo "<table>";
         echo "<tr>";
         echo "<th>Id</th>";
         echo "<th>Tytuł</th>";
         echo "<th>Opis</th>";
         echo "<th>Data wygaśniecia</th>";
         echo "<th>Cena netto</th>";
         echo "<th>Podatek vat</th>";
         echo "<th>Ilość</th>";
         echo "<th>Status</th>";
         echo "<th>Kategoria</th>";
         echo "<th>Gabaryt</th>";
         echo "<th>Zdjęcie</th>";
         echo "</tr>";
         echo "<tr>";
         echo "<td><input    type='number' disabled            id='id'               name='id'               value=$id              ></td>";
         echo "<td><input    type='text'                       id='tytul'            name='tytul'            value=$tytul           ></td>";
         echo "<td><textarea type='text'                       id='opis'             name='opis'             value=$opis            ></textarea></td>";
         echo "<td><input    type='date'                       id='data_wygasniecia' name='data_wygasniecia' value=$data_wygasniecia></td>";
         echo "<td><input    type='number' step='.01'          id='cena netto'       name='cena netto'       value=$cena_netto      ></td>";
         echo "<td><input    type='number'                     id='podatek_vat'      name='podatek_vat'      value=$podatek_vat     ></td>";
         echo "<td><input    type='number'                     id='ilosc'            name='ilosc'            value=$ilosc           ></td>";
         echo "<td><input    type='number'                     id='status'           name='status'           value=$status          ></td>";
         echo "<td><input    type='number'                     id='kategoria'        name='kategoria'        value=$kategoria       ></td>";
         // echo "<td><input    type='text'                       id='kategoria'        name='kategoria'        value=$kategoria       ></td>";
         echo "<td><input    type='char'                       id='gabaryt'          name='gabaryt'          value=$gabaryt         ></td>";
         echo "<td><input    type='text'                       id='zdjecie'          name='zdjecie'          value=$zdjecie         ></td>";
         echo "</tr>";
         echo "</table>";
         echo "<button type='submit' name='save'>Zapisz</button>";
         echo "</form>";
     }


// Pozwala dodać nową podstronę
if (isset($_GET['add'])) {
    include("../cfg.php");
    $id = $_GET['add'];
    $update = true;
    echo "<h2>Dodaj produkty</h2>";
    echo $data_wygasniecia;
    echo FormularzPodstrony(NULL,'tytul','opis',date('Y-m-d'),0,23,1,1,1,'A','placeholder.png');
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $tytul = $_POST['tytul'];
        $opis = $_POST['opis'];
        $data_wygasniecia = $_POST['data_wygasniecia'];
        $cena_netto = $_POST['cena_netto'];
        $podatek_vat = $_POST['podatek_vat'];
        $ilosc = $_POST['ilosc'];
        $status = $_POST['status'];
        $kategoria = $_POST['kategoria'];
        $gabaryt = $_POST['gabaryt'];
        $zdjecie = $_POST['zdjecie'];
        mysqli_query($conn, "INSERT INTO produkty(tytul, opis, data_wygasniecia, cena_netto, podatek_vat, ilosc, status, kategoria, gabaryt, zdjecie) VALUES('$tytul', '$opis','$data_wygasniecia', $cena_netto, $podatek_vat, $ilosc, $status, $kategoria, '$gabaryt', '$zdjecie')");
    }
}

// Pozwala edytować podstonę
if (isset($_GET['edit'])) {
    include("../cfg.php");
    $id = $_GET['edit'];
    $update = true;
    $record = mysqli_query($conn, "SELECT * FROM produkty WHERE id=$id");
    foreach ($record as $row) {
        echo "<h2>Edytuj produkty</h2>";
        echo FormularzPodstrony($row["id"],$row["tytul"],$row["opis"],$row["data_wygasniecia"],$row["cena_netto"],$row["podatek_vat"],$row["ilosc"],$row["status"],$row["kategoria"],$row["gabaryt"],$row["zdjecie"]);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];
            $tytul = $_POST['tytul'];
            $opis = $_POST['opis'];
            $data_modyfikacji = date('Y-m-d');
            $data_wygasniecia = $_POST['data_wygasniecia'];
            $cena_netto = $_POST['cena_netto'];
            $podatek_vat = $_POST['podatek_vat'];
            $ilosc = $_POST['ilosc'];
            $status = $_POST['status'];
            $kategoria = $_POST['kategoria'];
            $gabaryt = $_POST['gabaryt'];
            $zdjecie = $_POST['zdjecie'];
            // mysqli_query($conn, "UPDATE produkty SET tytul='$tytul', opis='$opis', data_modyfikacji='$data_modyfikacji', data_wygasniecia='$data_wygasniecia', cena_netto=$cena_netto, podatek_vat=$podatek_vat, ilosc=$ilosc, status=$status, kategoria='$kategoria', gabaryt='$gabaryt', zdjecie='$zdjecie' WHERE id=$row[id]");
            mysqli_query($conn, "UPDATE produkty SET tytul='$tytul',opis='$opis',data_modyfikacji='$data_modyfikacji',data_wygasniecia='$data_wygasniecia',cena_netto=$cena_netto,podatek_vat=$podatek_vat,ilosc=$ilosc,status=$status,kategoria=$kategoria,gabaryt='$gabaryt',zdjecie='$zdjecie' WHERE id=$row[id]");
        }
    }
}

// Pozwala usuwać produkt
if (isset($_GET['delete'])) {
    include("../cfg.php");
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM produkty WHERE id=$id");
}

// Wyświetla podstrony
PokazProdukty();
?>
    </div>
        </body>
        </html>

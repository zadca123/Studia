<?php
include("../cfg.php");

class Kategorie{
    public $id,$matka,$nazwa;
    public function __construct($id = NULL){
        if ($id != NULL){
            include("../cfg.php");
            $result = mysqli_query($conn, "SELECT * FROM kategorie WHERE id=$id");
            $kategoria = $result->fetch_object();
            $this->id              = $kategoria->id;
            $this->matka           = $kategoria->matka;
            $this->nazwa           = $kategoria->nazwa;
            mysqli_free_result($result);
                mysqli_close($conn);
            }else{
                $this->id              = 0;
                $this->matka           = 1;
                $this->nazwa           = 'kategoria placeholder';
            }
    }

    // gettery
    function getId(){
        return $this->id;
    }
    function getMatka(){
        return $this->matka;
    }
    function getNazwa(){
        return $this->nazwa;
    }

    // settery
    function setId($id){
        $this->id = $id;
    }
    function setMatka($matka){
        $this->matka = $matka;
    }
    function setNazwa($nazwa){
        $this->nazwa = $nazwa;
    }
}

function FormularzKategorii($kategoria){
    echo '<form method="post">
  <table>
    <tr>
      <th>Id</th>
      <th>Matka</th>
      <th>Nazwa</th>
    </tr>
    <tr>
      <td><input type="number" disabled   id="id"    name="id"    value=' . $kategoria->getId()    . '></td>
      <td><input type="number"            id="matka" name="matka" value=' . $kategoria->getMatka() . '></td>
      <td><input type="text"              id="nazwa" name="nazwa" value=' . $kategoria->getNazwa() . '></td>
    </tr>
  </table>
  <button type="submit" name="save">Zapisz</button>
</form>';
}

// Pozwala usuwać podstonę
if (isset($_GET['delete'])) {
    $kategoria = new Kategorie(mysqli_real_escape_string($conn, $_GET['delete']));
    mysqli_query($conn, 'DELETE FROM kategorie WHERE id='. $kategoria->getId());
}

// Pozwala edytować podstonę
if (isset($_GET['edit'])) {
    $kategoria = new Kategorie(mysqli_real_escape_string($conn, $_GET['edit']));
    echo FormularzKategorii($kategoria);
    echo '<h2>Edytuj kategorie</h2>';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $kategoria->setMatka($_POST['matka']);
        $kategoria->setNazwa($_POST['nazwa']);
        mysqli_query($conn, "UPDATE kategorie SET matka='" . $kategoria->getMatka() . "', nazwa='" . $kategorie->getNazwa() . "' WHERE id='" . $kategorie->getId() . "'");
    }
}

// Pozwala dodać nową podstronę
if (isset($_GET['add'])) {
    $kategoria = new Kategorie(mysqli_real_escape_string($conn, $_GET['add']));
    echo "<h2>Dodaj kategorie</h2>";
    echo FormularzKategorii($kategoria);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $kategoria->setMatka($_POST['matka']);
        $kategoria->setNazwa($_POST['nazwa']);
        mysqli_query($conn, "INSERT INTO kategorie (matka, nazwa) VALUES ('" . $kategiria->getMatka() . "','" . $kategoria->getNazwa() ."')");
    }
}

?>

    <!DOCTYPE html>
        <html lang="en">
    <head>
    <meta charset="utf-8">
    <link   href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
    <h1>Panel CMS</h1>
    <a href="verify.php">Wyloguj</a><br>
    <h2>Pokaz kategorie</h2>
    <table>
    <tr>
    <th>id</th>
    <th>matka</th>
    <th>nazwa</th>
    </tr>
<?php
        $result = mysqli_query($conn, "SELECT * FROM kategorie");
    if ($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            ?>
            <tr>
                   <td><?php echo $row['id']; ?></td>
                                                      <td><?php echo $row['matka']; ?></td>
                                                                                            <td><?php echo $row['nazwa']; ?></td>
                                                                                                                                  <td><a href="?edit=<?php echo $row['id']; ?>">EDYTUJ</a></td>
                                                                                                                                  <td><a href="?delete=<?php echo $row['id']; ?>">USUŃ</a></td>
                                                                                                                                  </tr>
<?php } ?>
                                                                                                                                  </table>
                                                                                                                                        <a href="?add=0">DODAJ</a>
<?php } else { echo '0 results';} $conn->close(); ?>
    </div>
        </body>
        </html>

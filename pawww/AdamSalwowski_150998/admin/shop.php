<!DOCTYPE HTML>
    <html lang="en">
     <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="../css/shop.css">
     </head>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <script>
     // do usuwania rekordów z tabeli (tylko graficznie)
     function SomeDeleteRowFunction(o) {
         var p=o.parentNode.parentNode;
         p.parentNode.removeChild(p);
     }
     // popup window po dodaniu produktu
     $( function() {
         $( "#dialog" ).dialog({
                 autoOpen: false,
                       show: {
                     effect: "blind",
                       duration: 1000
                       },
                       hide: {
                     effect: "explode",
                       duration: 1000
                       }
             });
    
         $( "#opener" ).on( "click", function() {
             $( "#dialog" ).dialog( "open" );
         });
     } );
</script>

    <body>
    <title>Sklep</title>
    <div class="container">
     <div id='dialog' title='Wiadomość '>
     <p>Pomyślnie dodano produkt do koszyka!</p>
                                        </div>
                                        <h1 align='center'>Sklep</h1>
                                        <a href="../index.php?idp=glowna">Wstecz</a><br>
<?php
                                        session_start();

function PokazProduktySklepu(){
    include("../cfg.php");
    $result = $conn->query("SELECT * FROM produkty");
    echo "<table class='table'>";
    echo "<thead class='thead-primary'>";
    echo "<tr>";
    echo "<th>Nazwa</th>";
    echo "<th>Opis</th>";
    echo "<th>Cena</th>";
    echo "<th>Zdjecie</th>";
    echo "<th>Ilosc</th>";
    echo "<th>&nbsp;</th>";
    echo "<th>&nbsp;</th>";
    echo "</tr>";
    echo "</thead>";
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $cena_brutto = ($row["cena_netto"] + ($row["cena_netto"] * $row["podatek_vat"]));
            $suma += ($cena_brutto);
            echo "<tr>";
            echo "<td>" . $row["tytul"] . "</td>";
            echo "<td>" . $row["opis"] . "</td>";
            echo "<td>" . ($row["cena_netto"] + ($row["cena_netto"] * $row["podatek_vat"])) . "zł</td>";
            echo "<td><img width=80 height=80 src='../img/" . $row["zdjecie"] . "'/></td>";
            echo '<td class="quantity">';
            echo ' <div class="input-group">';
            echo " <input type='text' name='quantity' class='quantity form-control input-number' value='1' min='1' max=" . $row['ilosc'] . ">";
            echo '</div>';
            echo '</td>';

            echo '<td>';
            echo "<input type='submit' id='opener' value='Dodaj do koszyka'>";
            echo '</td>';

            echo "<td><input type='button' value='Usuń z koszyka' onclick='SomeDeleteRowFunction(this)'></td>";
            // echo "<td><a href='?delete=" . $row['id'] . "'>Usun z koszyka</a></td>";
            echo "</tr>";
        }
        echo "<thead class='thead-primary'>";
        echo "<tr>";
	    echo "<th>&nbsp;</th>";
	    echo "<th>Do zapłaty: </th>";
	    echo "<th><b>" . $suma . " zł<b></th>";
	    // echo "<th>Ilosc</th>";
	    echo "<th>&nbsp;</th>";
	    echo "<th>&nbsp;</th>";
	    echo "<th>&nbsp;</th>";
	    echo "<th>&nbsp;</th>";
	    echo "</tr>";
        echo "</thead>";
        echo "</table>";
    }
    $conn->close();
}

// Wyświetla podstrony
PokazProduktySklepu();
?>
    </div>
        </body>              
        </html>

<?php

$nr_indeksu='150998';
$nr_grupy='3';

// echo 'Adam Salwowski, indeks: ' $nr_indeksu ', grupa: ' $nr_grupy '<br/><br/>';
// To wygląda ładniej i jest łatwiejsze do zrozumienia
echo "Adam Salwowski, indeks: $nr_indeksu, grupa:  $nr_grupy <br><br>";

echo 'Podpunkt a)</br>';
// echo "$fruit $color";
include('vars.php'); // wczytuje plik za każdym razem, jeśli go brak, wyrzuca ostrzerzenie (warning)
echo "$fruit has the $color color<br>";
require_once('vars.php'); //wczytuje plik tylko raz, jeśli go brak wyrzuca błąd
echo "$fruit has the $color color<br>";

echo '<br>Podpunkt b) oraz c)<br>';
if ($num1 > $num2)
    echo "$num1 jest większa niż $num2<br>";
elseif($num1 == $num2)
    echo "$num1 jest równa $num2<br>";
else
    echo "$num1 jest mniejsza niż $num2<br>";

for($i=0; $i < 10; $i++){
    echo "zmienna i ma wartość: $i<br>";
    switch ($i) {
        case $num1:
            echo "zmienna i ma wartość równą zmiennej num1<br>";
            break;
        case $num2:
            echo "zmienna i ma wartość równą zmiennej num2<br>";
            break;
    }
}

while($i != 0){
    echo "Obecna wartość: $i<br>";
    $i--;
}

echo '<br>Podpunkt d)<br>';
// Proszę przetestować tę część kodu na zakładce kontakt !!!
echo "Witaj " . $_GET['cos1'] . " na " . $_GET['cos2'];
echo '<br>Twoje imię to: ' . $_POST['fname'];
echo '<br>Twoje nazwisko to: ' . $_POST['lname'];
echo '<br>Twój  wiek to: ' . $_POST['age'];

session_start();
$_SESSION['imie'] = $_POST['fname'];
$_SESSION['nazwisko'] = $_POST['lname'];
echo '<br>' . $_SESSION["imie"];
echo '<br>' . $_SESSION["nazwisko"];
?>

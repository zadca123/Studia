var computed = false;
var decimal = 0;

function convert(entryform, from, to){
    convertFrom = from.selectedIndex;
    convertTo=to.selectedIndex;
    entryFrom.display.value = (entryFrom.input.value * from[convertFrom].value / to[convertTo].value);
}

function addCharacter(input, character){
    if ((character=='.' && decimal=="0") || character!='.'){
	(input.value=="" || input.value=="0") ? input.value=character : input.value += character
	convert(input.form,input.form.measure1,input.form.measure2)
	computed=True;

	if(character=='.')
	    decimal=1;
    }
}

function openVothcom(){
    window.open("", "Display window","toorbal=no,directories=no,menubar=no")
}

function clr(form){
    form.input.value=0;
    form.display.value=0;
    decimal=0;
}

function changeBackground(color){
    document.body.style.backgroundImage=color; // this deletes image
    document.body.style.backgroundColor=color;
}

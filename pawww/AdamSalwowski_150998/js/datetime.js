function getDate(){
    Todays=new Date();
    TheDate = "" + (Todays.getMonth()+1)+ " / " + Todays.getDate() + " / " + (Todays.getYear()-100);
    document.getElementById("data").innerHTML = TheDate;
}
var counterID = null;
var counterRunning = false;

function stopclock(){
    if(counterRunning)
	clearTimeout(counterID);
    counterRunning = false;
}

function startclock(){
    stopclock();
    getDate();
    showTime();
}

function showTime(){
    var now = new Date();
    var hours = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    var timeValue = "" + ((hours>12) ? hours -12 :hours)

    timeValue += ((minutes<10) ? ":0" : ":") + minutes
    timeValue += ((seconds<10) ? ":0" : ":") + seconds
    timeValue += ((hours>=12) ? " P.M." : " A.M. ")

    document.getElementById("zegarek").innerHTML = timeValue;
    counterID = setTimeout("showTime()",1000);
    counterRunning = True;
}

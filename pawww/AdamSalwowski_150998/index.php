<?php
 include 'cfg.php';
 include 'showpage.php';
 error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
 $strona = PokazPodstrone('glowna');
 $strona = PokazPodstrone($_GET['idp']);
 ?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="pl" />
    <meta name="Author" content="Adam Salwowski" />
    <title>Moim hobby jest GNU\Linux</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/kolorujtlo.js" type="text/javascript"></script>
    <script src="js/datetime.js" type="text/javascript"></script>
    <script src="js/scrollNavbar.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body onload="startclock()"><br></br>
    <div class="topnav" id="navbar">
      <a href="index.php?idp=glowna">Strona główna</a>
      <a href="index.php?idp=blog">Blog</a>
      <a href="index.php?idp=galeria">Galeria</a>
      <a href="index.php?idp=kontakt">Kontakt</a>
      <a href="index.php?idp=faq">FAQ</a>
      <a href="index.php?idp=start">Start</a>
      <a href="index.php?idp=filmy">Filmy</a>
      <a href="admin/shop.php">Sklep</a>
      <a href="admin/login.php">Logowanie</a>
    </div>
    <div id="container">
      <div id="logo">
	<img height="200" src="img/tux.png" /><br>
	<h1 id="title"><i>Witam na stronie hobbysty <span style="color:var(--red)">GNU/Linux</span></i></h1>
      </div><br></br>
      <div id="content">
	<?php
	 echo $strona;
	 ?>
      </div><br></br>
      <footer>
	<h2 id="title">Wybierz kolor tła</h2><br>
	<form method="post" name="background">
	  <input type="button" , style="color:var(--yellow)" , value="zółty" , onclick="changeBackground('var(--yellow)')">
	  <input type="button" , style="color:var(--black)" , value="czarny" , onclick="changeBackground('var(--black)')">
	  <input type="button" , style="color:var(--white)" , value="biały" , onclick="changeBackground('var(--white)')">
	  <input type="button" , style="color:var(--green)" , value="zielony" , onclick="changeBackground('var(--green)')">
	  <input type="button" , style="color:var(--blue)" , value="niebieski" , onclick="changeBackground('var(--blue)')">
	  <input type="button" , style="color:var(--br-red)" , value="pomarańczowy" , onclick="changeBackground('var(--br-red)')">
	  <input type="button" , style="color:var(--br-black)" , value="szary" , onclick="changeBackground('var(--br-black)')">
	  <input type="button" , style="color:var(--red)" , value="czerwony" , onclick="changeBackground('var(--red)')">
	</form><br></br>
	<div id="data"></div>
	<div id="zegarek"></div>
      </footer>
    </div>
  </body>
</html>

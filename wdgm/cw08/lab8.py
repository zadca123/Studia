from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from PIL import ImageFilter
from PIL import ImageChops
from PIL import ImageOps
from PIL.ImageFilter import (
    BLUR, CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE,
    EMBOSS, FIND_EDGES, SMOOTH, SMOOTH_MORE, SHARPEN)
from PIL import ImageEnhance
import matplotlib.pyplot as plt


# im = Image.open('obraz.jpg')
# print("tryb obrazu", im.mode)
# print("rozmiar", im.size)

# #  Class   PIL.ImageFilter.Kernel(size, kernel, scale=None, offset=0) -- scale jest zazwyczaj sumą wyrazów w kernel, w algorytmie kernel normalizujemy dzieląc przez scale
# fe = im.filter(ImageFilter.FIND_EDGES) # filtruje obraz im
# # fe.show()
# print(ImageFilter.FIND_EDGES.filterargs)  # wyświetla argumenty, w tym rozmiar i  wartości tablicy Kernel

# ImageFilter.FIND_EDGES.filterargs = ((3, 3), 1, 0, (-1, -1, -1, -1, 6, -1, -1, -1, -1)) # pozwala zmieniac wartości listy Kernel, skalę i offset.
# fe1 = im.filter(ImageFilter.FIND_EDGES) # filtruje obraz im
# # fe1.show()

# ImageFilter.FIND_EDGES.filterargs = ((3, 3), 1, 0, (-1, -1, -1, -1, 8, -1, -1, -1, -1)) # pozwala zmieniac wartości listy Kernel, skalę i offset.
# fe3 = im.filter(ImageFilter.FIND_EDGES) # filtruje obraz im
# # fe3.show()
# fe_ker = im.filter(ImageFilter.Kernel(size = (3, 3), kernel = (-1, -1, -1, -1, 8, -1, -1, -1, -1), scale=1, offset=0))
# # fe_ker.show()
# roznica = ImageChops.difference(fe3, fe_ker)
# # roznica.show()

#############################################################

obraz = Image.open('obraz.jpg')
# Zadanie 1
def filtruj(obraz, kernel, scale):
    img = obraz.copy()
    tab_obraz = obraz.load()
    tab_img = img.load()
    w, h = obraz.size
    n = len(kernel)
    for x in range(int(n / 2), w - int(n / 2)):
        for y in range(int(n / 2), h - int(n / 2)):
            temp = [0, 0, 0]
            for i in range(n):
                for j in range(n):
                    i_n = x + i - int(n / 2)
                    j_n = y + j - int(n / 2)
                    pixel = tab_obraz[i_n, j_n]
                    temp[0] += pixel[0] * kernel[i][j]
                    temp[1] += pixel[1] * kernel[i][j]
                    temp[2] += pixel[2] * kernel[i][j]
            tab_img[x, y] = (int(temp[0] / scale), int(temp[1] / scale), int(temp[2] / scale))
    return img
# img = filtruj(obraz, [[-1,1,-1],[-1,8,-1],[-1,-1,-1]], 1)
# img.show()

# def filtruj(obraz, kernel_, scale_):
#     return obraz.filter(ImageFilter.Kernel(size=(3, 3), kernel=kernel_, scale=scale_))
# img = filtruj(obraz, (-1, -1, -1, -1, 8, -1, -1, -1, -1), 1)
# img.show()

# Zadanie 2
blur = obraz.filter(ImageFilter.BLUR)
# blur = filtruj(blur, (-1, 1, -1, -1, 8, -1, -1, -1, -1), 1)
blur = filtruj(blur, [[-1,1,-1],[-1,8,-1],[-1,-1,-1]], 1)
blur.save("blur.jpg")
# blur.show()

# blur = obraz.filter(ImageFilter.BLUR)
# print(ImageFilter.BLUR.filterargs)
# blur = filtruj(obraz, [[1, 1, 1, 1, 1], [1, 0, 0, 0, 1], [1, 0, 0, 0, 1], [1, 0, 0, 0, 1], [1, 1, 1, 1, 1]], ImageFilter.BLUR.filterargs[1])
# blur.show()



# 2.a
blur1 = obraz.filter(ImageFilter.BLUR)
blur1.save("blur1.jpg")
# blur1.show()

# 2.b
roznica = ImageChops.difference(blur, blur1)
# roznica.show()

# Zadanie 3
# filtr DETAIL
filtr1_1 = obraz.filter(ImageFilter.DETAIL)
# filtr1_1 = filtruj(filtr1_1, (-1, 1, -1, -1, 8, -1, -1, -1, -1), 1)
filtr1_1 = filtruj(filtr1_1, [[-1,1,-1],[-1,8,-1],[-1,-1,-1]], 1)
filtr1_1.save("filtr1_1.jpg")
# filtr1_1.show()
filtr1_2 = obraz.filter(ImageFilter.DETAIL)
filtr1_2.save("filtr1_2.jpg")
# filtr1_2.show()
roznica = ImageChops.difference(filtr1_1, filtr1_2)
# roznica.show()

# # filtr EDGE_ENHANCE
filtr2_1 = obraz.filter(ImageFilter.EDGE_ENHANCE)
# filtr2_1 = filtruj(filtr2_1, (-1, 1, -1, -1, 8, -1, -1, -1, -1), 1)
filtr2_1 = filtruj(filtr2_1, [[-1,1,-1],[-1,8,-1],[-1,-1,-1]], 1)
filtr2_1.save("filtr2_1.jpg")
# filtr2_1.show()
filtr2_2 = obraz.filter(ImageFilter.EDGE_ENHANCE)
filtr2_2.save("filtr2_2.jpg")
# filtr2_2.show()
roznica = ImageChops.difference(filtr2_1, filtr2_2)
# roznica.show()

# Zadanie 4
emboss = obraz.filter(ImageFilter.EMBOSS)
emboss = filtruj(emboss, [[-1,1,-1],[-1,8,-1],[-1,-1,-1]], 1)
emboss.save("emboss.jpg")
# emboss.show()

# Zadanie 5
obraz_L = obraz.convert("L")
print (ImageFilter.EMBOSS.filterargs)

# 5.a
# sobel1 = filtruj(obraz_L, [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], 1)
ImageFilter.EMBOSS.filterargs = ((3, 3), 1, 128, (-1, 0, 1, -2, 0, 2, -1, 0, 1))
sobel1 = obraz_L.filter(ImageFilter.EMBOSS)
sobel1.save("sobel1.jpg")
# sobel1.show()

# 5.b
ImageFilter.EMBOSS.filterargs = ((3, 3), 1, 128, (-1, -2, -1, 0, 0, 0, 1, 2, 1))
sobel2 = obraz_L.filter(ImageFilter.EMBOSS)
sobel2.save("sobel2.jpg")
# sobel2.show()

# 5.c
# sobel2 jest bardziej wypukły od sobel1, gdzie w sobel1 wypukłe wydają się być tylko krawędzie

# Zadanie 6
filtr01 = obraz.filter(ImageFilter.EDGE_ENHANCE_MORE)
filtr02 = obraz.filter(ImageFilter.FIND_EDGES)
filtr03 = obraz.filter(ImageFilter.SHARPEN)
filtr04 = obraz.filter(ImageFilter.SMOOTH)
filtr05 = obraz.filter(ImageFilter.SMOOTH_MORE)
filtr06 = obraz.filter(ImageFilter.CONTOUR)
filtr07 = obraz.filter(ImageFilter.BoxBlur(0))
filtr08 = obraz.filter(ImageFilter.GaussianBlur(radius=3))
filtr09 = obraz.filter(ImageFilter.UnsharpMask(radius=2,percent=150,threshold=3))
filtr10 = obraz.filter(ImageFilter.Kernel((3,3),(-1,-1,-1,-1,10,-2,-2,-2,-2),1,0))
filtr11 = obraz.filter(ImageFilter.RankFilter(size=3,rank=0))
filtr12 = obraz.filter(ImageFilter.MedianFilter(size=3))
filtr13 = obraz.filter(ImageFilter.MinFilter(size=3))
filtr14 = obraz.filter(ImageFilter.MaxFilter(size=3))

plt.figure(figsize=(32, 16))
plt.subplot(4,4,1) # ile obrazów w pionie, ile w poziomie, numer obrazu
plt.imshow(filtr01)
plt.axis('off')
plt.subplot(4,4,2)
plt.imshow(filtr02)
plt.axis('off')
plt.subplot(4,4,3)
plt.imshow(filtr03)
plt.axis('off')
plt.subplot(4,4,4)
plt.imshow(filtr04)
plt.axis('off')
plt.subplot(4,4,5)
plt.imshow(filtr05)
plt.axis('off')
plt.subplot(4,4,6)
plt.imshow(filtr06)
plt.axis('off')
plt.subplot(4,4,7)
plt.imshow(filtr07)
plt.axis('off')
plt.subplot(4,4,8)
plt.imshow(filtr08)
plt.axis('off')
plt.subplot(4,4,9)
plt.imshow(filtr09)
plt.axis('off')
plt.subplot(4,4,10)
plt.imshow(filtr10)
plt.axis('off')
plt.subplot(4,4,11)
plt.imshow(filtr11)
plt.axis('off')
plt.subplot(4,4,12)
plt.imshow(filtr12)
plt.axis('off')
plt.subplot(4,4,13)
plt.imshow(filtr13)
plt.axis('off')
plt.subplot(4,4,14)
plt.imshow(filtr14)
plt.axis('off')
plt.subplots_adjust(wspace=0.05, hspace=0.05)
plt.savefig('reszta.jpg', bbox_inches='tight', dpi=150)
# plt.show()

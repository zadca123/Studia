from PIL import Image  # Python Imaging Library
import numpy as np


# obrazy w odcieniach szarości
def rysuj_ramke_szare(w, h, dzielnik, kolor):
    t = (h, w)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)  # deklaracja tablicy
    tab[:] = 100  # wypełnienie tablicy szarym kolorem o wartości 100
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości  ramki
    z1 = h - grub
    z2 = w - grub
    tab[grub:z1, grub:z2] = kolor  # wypełnienie tablicy kolorem o wartości kolor
    return tab

tab = rysuj_ramke_szare(120, 60, 8, 200)
im_ramka = Image.fromarray(tab)
im_ramka.show()


def rysuj_pasy_poziome_szare(w, h, dzielnik, zmiana_koloru):  # w, h   -  rozmiar obrazu
    t = (h, w)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    grub = int(h / dzielnik)  # liczba pasów = 9 o grubości 10
    for k in range(dzielnik):  # uwaga k = 0,1,2,3,4,5,8   bez dziewiatki
        for g in range(grub):
            i = k * grub + g  # i - indeks wiersza, j - indeks kolumny
            for j in range(w):
                tab[i, j] = (k * zmiana_koloru) % 256  # reszta z dzielenia przez dwa
    return tab


tab1 = rysuj_pasy_poziome_szare(300, 200, 100, 10)
obraz1 = Image.fromarray(tab1)
obraz1.show()


def negatyw_szare(tab):  # tworzy tablicę dla negatywu
    h, w = tab.shape
    tab_neg = tab.copy()
    for i in range(h):
        for j in range(w):
            tab_neg[i, j] = 255 - tab[i, j]
    return tab_neg


tab_neg = negatyw_szare(tab1)
obraz_neg = Image.fromarray(tab_neg)
obraz_neg.show()


def rysuj_po_przekatnej_szare(w):  # rysuje kwadratowy obraz
    t = (w, w)
    tab = np.zeros(t, dtype=np.uint8)
    for i in range(w):
        for j in range(w):
            tab[i, j] = (i + j) % 256
    return tab


tab = rysuj_po_przekatnej_szare(512)
im = Image.fromarray(tab)
im.show()


def rysuj_ramki_szare(w):
    t = (w, w)
    tab = np.zeros(t, dtype=np.uint8)
    kolor = 255 - int(w / 2) + 1
    z = w
    for k in range(int(w / 2)):
        for i in range(k, z - k):
            for j in range(k, z - k):
                tab[i, j] = kolor
        kolor = kolor + 1
    return tab


tab = rysuj_ramki_szare(300)
im = Image.fromarray(tab)
im.show()


# obrazy kolorowe
def rysuj_ramke_kolor(w, h, dzielnik, r, g, b):
    t = (h, w, 3)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)  # deklaracja tablicy
    tab[:] = [255, 0, 0]  # wypełnienie tablicy kolorem czerwonym
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości  ramki
    z1 = h - grub
    z2 = w - grub
    tab[grub:z1, grub:z2, 0] = r  # wypełnienie wartości kanału R liczbą r
    tab[grub:z1, grub:z2, 1] = g  # wypełnienie wartości kanału G liczbą g
    tab[grub:z1, grub:z2, 2] = b  # wypełnienie wartości kanału R liczbą r
    # tab[grub:z1, grub:z2] = [r,g,b] # wersja równoważna
    return tab


tab = rysuj_ramke_kolor(120, 60, 8, 0, 200, 100)
im_ramka = Image.fromarray(tab)
im_ramka.show()


def rysuj_pasy_poziome_3kolory(w, h, dzielnik):  # funkcja rysuje pasy poziome na przemian czerwony, zielony, niebieski
    t = (h, w, 3)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    grub = int(h / dzielnik)  # liczba pasów = 9 o grubości 10
    for k in range(dzielnik):  # uwaga k = 0,1,2,3,4,5,8   bez dziewiatki
        for g in range(grub):
            i = k * grub + g
            for j in range(w):
                if k % 3 == 0:
                    tab[i, j] = [255, 0, 0]
                elif k % 3 == 1:
                    tab[i, j] = [0, 255, 0]
                else:
                    tab[i, j] = [0, 0, 255]
    return tab


tab1 = rysuj_pasy_poziome_3kolory(300, 200, 20)
obraz1 = Image.fromarray(tab1)
obraz1.show()


def rysuj_pasy_poziome_kolor(w, h, dzielnik, kolor,
                             zmiana_koloru):  # funkcja rysuje pasy poziome, przy czym kazda składowa koloru zwieksza się o "zmiana_koloru"
    t = (h, w, 3)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    tab[:] = kolor
    grub = int(h / dzielnik)
    for k in range(dzielnik):
        r = (kolor[0] + k * zmiana_koloru) % 255
        g = (kolor[1] + k * zmiana_koloru) % 255
        b = (kolor[2] + k * zmiana_koloru) % 255
        for m in range(grub):
            i = k * grub + m
            for j in range(w):
                tab[i, j] = [r, g, b]
    return tab


tab1 = rysuj_pasy_poziome_kolor(300, 200, 20, [100, 200, 0], 32)
obraz1 = Image.fromarray(tab1)
obraz1.show()

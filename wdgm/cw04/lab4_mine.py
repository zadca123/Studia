from PIL import Image  # Python Imaging Library
import numpy as np

# Zadanie 1
def rysuj_pasy_pionowe(w, h, dzielnik, kolor):
    # użytkownik sam ustawia jeden kolor
    t = (h, w)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    grub = int(w / dzielnik) 
    tab[:] = 100
    for k in range(dzielnik):
        for g in range(grub):
            i = k * grub + g
            for j in range(h):
                if k%2 != 0:
                    tab[j, i] = kolor
    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz1.bmp")
    # obraz.show()
rysuj_pasy_pionowe(600, 200, 20, 200)

# podpunkt 1.1 oraz 1.2
def negatyw_szare(tab):  # tworzy tablicę dla negatywu
    h, w = tab.shape
    tab_neg = tab.copy()
    for i in range(h):
        for j in range(w):
            tab_neg[i, j] = 255 - tab[i, j]
    return tab_neg
obraz1=Image.open("zad1_obraz1.bmp")
obraz1_tab=np.array(obraz1, dtype=np.uint8)
obraz1_tab_neg=negatyw_szare(obraz1_tab)
obraz1_neg=Image.fromarray(obraz1_tab_neg)
obraz1_neg.save("zad1_obraz1_neg.jpg")
# obraz1_neg.show()

# Zadanie 2
def rysuj_ramke(w, h, dzielnik, r,g,b):
    # użytkownik sam ustawia kolory
    t = (h, w, 3)  # rozmiar tablicy
    tab = np.zeros(t, dtype=np.uint8)  # deklaracja tablicy wypełnionej zerami - czarna
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości  ramki
    z1 = h - grub
    z2 = w - grub
    tab[:] = [r,0,0]
    for i in range(1, int(h / grub)+ 1):
        if i % 2 == 1:
            tab[i * grub:z1 - (i-1) * grub, i * grub:z2 - (i-1) * grub] = [0,g,0]
        else:
            tab[i * grub:z1 - (i-1) * grub, i * grub:z2 - (i-1) * grub] = [0,0,b]
    obraz = Image.fromarray(tab)
    obraz.save('zad2_obraz1.bmp')
    # obraz.show()
rysuj_ramke(120, 60, 8, 255,255,255)

#podpunkt 2.1
def negatyw_rgb(tab):  # tworzy tablicę dla negatywu
    h, w, t = tab.shape
    tab_neg = tab.copy()
    for i in range(h):
        for j in range(w):
            for k in range(t):
                tab_neg[i, j, k] = 255 - tab[i, j, k]
    return tab_neg
obraz2=Image.open("zad2_obraz1.bmp")
obraz2_tab=np.array(obraz2, dtype=np.uint8)
obraz2_tab_neg=negatyw_rgb(obraz2_tab)
obraz2_neg=Image.fromarray(obraz2_tab_neg)
obraz2_neg.save("zad2_obraz1_neg.jpg")
# obraz2_neg.show()

# Zadanie 3
def inicjaly_paski(tab,dzielnik,r,g,b):
    # użytkownik sam ustawia kolory oraz ile razy mają się powtarzać
    h,w = tab.shape
    t = (h, w, 3)
    tab_kolor = np.ones(t, dtype=np.uint8)
    grub = int(h / dzielnik)  # liczba pasów = 9 o grubości 10
    for k in range(dzielnik):  # uwaga k = 0,1,2,3,4,5,8   bez dziewiatki
        for g in range(grub):
            i = k * grub + g
            for j in range(w):
                if k % 3 == 0:
                    tab_kolor[i, j] = [r, 0, 0]
                elif k % 3 == 1:
                    tab_kolor[i, j] = [0, g, 0]
                else:
                    tab_kolor[i, j] = [0, 0, b]
    for y in range(h):
        for x in range(w):
            if tab[y,x]==1:
                tab_kolor[y,x]=[255,255,255]
    obraz = Image.fromarray(tab_kolor)
    obraz.save("zad3_inicjaly_paski_rgb.jpg")
    # obraz.show()
obraz3 = Image.open("inicjaly.bmp")
obraz3_tab = np.array(obraz3, dtype=np.uint8)
inicjaly_paski(obraz3_tab,10,255,0,255)

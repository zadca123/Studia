# Stwórz kratownicę z macierzy. Pierwszy pikel w lewym górnym rogu ma być czarny.

width = 200
height = 200

A = []
for y in range(height):
    a=[]
    for x in range(int(width/2)):
        if y % 2 == 0:
            a.extend([1,0])
        else:
            a.extend([0,1])
    A.append(a)
        
for i in A:
    print(i)

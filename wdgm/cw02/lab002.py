
from PIL import Image   # Python Imaging Library
import numpy as np

# ZADANIE 1
obrazek = Image.open("obrazek.bmp") # wczytywanie obrazu
print("typ", obrazek.mode)
print("format", obrazek.format)
print("rozmiar", obrazek.size)

obrazek2 = Image.open("obrazek2.jpg") # wczytywanie obrazu
print("typ", obrazek2.mode)
print("format", obrazek2.format)
print("rozmiar", obrazek2.size)

obrazek3 = Image.open("obrazek3.png") # wczytywanie obrazu
print("typ", obrazek3.mode)
print("format", obrazek3.format)
print("rozmiar", obrazek3.size)

dane_obrazka = np.array(obrazek)
print("typ danych tablicy", dane_obrazka.dtype)  # typ danych przechowywanych w tablicy
print("rozmiar tablicy", dane_obrazka.shape)  # rozmiar tablicy - warto porównac z wymiarami obrazka
print("liczba elementow", dane_obrazka.size)  # liczba elementów tablicy
print("wymiar tablicy", dane_obrazka.ndim) # wymiar mówi czy to jest talica 1D, 2d, 3D ...
print("wymiar wyrazu tablicy", dane_obrazka.itemsize)  # pokazuje ile współrzednych trzeba do opisania wyrazu tablicy (piksela)
print("pierwszy wyraz", dane_obrazka[0][0])
print("drugi wyraz", dane_obrazka[1][0])
print("***************************************")
print(dane_obrazka)
dane_obrazka2 = np.asarray(obrazek2)
print("typ danych tablicy", dane_obrazka2.dtype)  # typ danych przechowywanych w tablicy
print("rozmiar tablicy", dane_obrazka2.shape)  # rozmiar tablicy - warto porównac z wymiarami obrazka
print("liczba elementow", dane_obrazka2.size)  # liczba elementów tablicy
print("wymiar tablicy", dane_obrazka2.ndim) # wymiar mówi czy to jest talica 1D, 2d, 3D ...
print("wymiar wyrazu tablicy", dane_obrazka2.itemsize)  # pokazuje ile współrzednych trzeba do opisania wyrazu tablicy (piksela)
print("pierwszy wyraz", dane_obrazka2[0][0])
print("drugi wyraz", dane_obrazka2[1][0])
print("***************************************")
print(dane_obrazka2)
dane_obrazka3 = np.asarray(obrazek3)
print("typ danych tablicy", dane_obrazka3.dtype)  # typ danych przechowywanych w tablicy
print("rozmiar tablicy", dane_obrazka3.shape)  # rozmiar tablicy - warto porównac z wymiarami obrazka
print("liczba elementow", dane_obrazka3.size)  # liczba elementów tablicy
print("wymiar tablicy", dane_obrazka3.ndim) # wymiar mówi czy to jest talica 1D, 2d, 3D ...
print("wymiar wyrazu tablicy", dane_obrazka3.itemsize)  # pokazuje ile współrzednych trzeba do opisania wyrazu tablicy (piksela)
print("pierwszy wyraz", dane_obrazka3[0][0])
print("drugi wyraz", dane_obrazka3[1][0])
print("***************************************")
print(dane_obrazka3)
# obrazy różnią się formatem (png,jpeg,bmp)

### ZADANIE 2
nowa_t1 = np.loadtxt("dane2.txt", dtype = np.bool_ )
# nowa_t1 *= 255
print(nowa_t1)
print("***************************************")
print(dane_obrazka)
porownanie = nowa_t1 == dane_obrazka
### brak atrybutu all?
czy_rowne = porownanie.all()
print(czy_rowne)
print(np.sum(dane_obrazka))
print(np.sum(nowa_t1))

### ZADANIE 3
obraz = Image.fromarray(nowa_t1)
obraz.show()

### ZADANIE 4
t2 = np.loadtxt("dane2.txt", dtype = np.uint8 )
obraz2 = Image.fromarray(t2 * 255)
print("TYP: ", obraz2.mode)
obraz2.show()

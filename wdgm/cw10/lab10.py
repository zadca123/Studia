# import numpy as np
# from PIL.ImageFilter import *
# from resizeimage import resizeimage
import matplotlib.pyplot as plt
from PIL import Image
import sys

obraz = Image.open("obraz.jpg")

# # Zadanie 1
def zmien_rozmiar(obraz, s1, s2, filtr):
    w, h = obraz.size
    if s1 > 5 or s2 > 5:
        sys.exit("Większe wartości mogą spowodować zawieszenie się komputera!@!")
    w = int(w*s1)
    h = int(h*s2)
    return obraz.resize((w, h), filtr)
# zmien_rozmiar(obraz, 0.2, 0.2, 0).show()
# zmien_rozmiar(obraz, 6, 5, 1).show()

# # Zadanie 2
img00 = zmien_rozmiar(obraz, 0.5, 0.5, Image.NEAREST)
img01 = zmien_rozmiar(obraz, 0.5, 0.5, Image.LANCZOS)
img02 = zmien_rozmiar(obraz, 0.5, 0.5, Image.BILINEAR)
img03 = zmien_rozmiar(obraz, 0.5, 0.5, Image.BICUBIC)
img04 = zmien_rozmiar(obraz, 0.5, 0.5, Image.BOX)
img05 = zmien_rozmiar(obraz, 0.5, 0.5, Image.HAMMING)

images = [img00,img01,img02,img03,img04,img05]
figure = plt.figure(figsize=(32, 32))
for i in range(len(images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(images[i])
plt.savefig("resize.jpg")
# plt.show()

# # Zadanie 3
glowa = obraz.resize(size=obraz.size, box=(300,20, 500, 150))
glowa = glowa.resize((glowa.size[0]*2,glowa.size[1]*3))
glowa.save("glowa.jpg")
# glowa.show()

# # Zadanie 4
obrot1 = obraz.rotate(angle=60, fillcolor="red", expand=1) 
obrot1.save("obrot1.jpg")
# obrot1.show()

# # Zadanie 5
obrot2 = obraz.rotate(angle=-30, fillcolor="yellow")
obrot2.save("obrot2.jpg")
# obrot2.show()

# # Zadanie 6
# punkt obrotu to oko
obrot3 = obraz.rotate(angle=45, fillcolor="pink", center=(390,75))
obrot3.save("obrot3.jpg")
# obrot3.show()
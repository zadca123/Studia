from PIL import Image   # Python Imaging Library
import numpy as np

# Zadanie 1
obrazek = Image.open("obrazek.bmp")
obrazek2 = Image.open("obrazek2.jpg")
obrazek3 = Image.open("obrazek3.png")

# print("typ", obrazek.mode)
# print("format", obrazek.format)
# print ("rozmiar", obrazek.size)

# print("typ", obrazek2.mode)
# print("format", obrazek2.format)
# print ("rozmiar", obrazek2.size)

# print("typ", obrazek3.mode)
# print("format", obrazek3.format)
# print ("rozmiar", obrazek3.size)
# obrazki różnią się formatem

### Zadanie 2
tab_obr = np.asarray(obrazek, dtype= np.bool_ ) * 1
tab_txt_bool = np.loadtxt("dane.txt", dtype=np.bool_ ) * 1
print (tab_obr)
print('czesc')
print (tab_txt_bool)
porownanie = tab_obr == tab_txt_bool
print (porownanie)
# nie są równe :(

# ### Zadanie 3
tab_txt = np.loadtxt("dane.txt")
obraz_z_txt = Image.fromarray(tab_txt)
# obraz_z_txt.show()

# ### Zadanie 4
tab_txt = np.loadtxt("dane.txt", dtype=np.uint8)
obraz_z_txt = Image.fromarray(tab_txt)
obraz_z_txt.show()

### Zad 5
def rysuj_ramke(w, h, dzielnik):
    t = (h, w)  # rozmiar tablicy
    tab = np.zeros(t, dtype=np.uint8)  # deklaracja tablicy wypełnionej zerami - czarna
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości  ramki
    z1 = h - grub
    z2 = w - grub
    tab[grub:z1, grub:z2] = 1  # skrócona wersja ustawienia wartości dla prostokatnego fragmentu tablicy [zakresy wysokości, zakresy szerokości] tablicy
    return tab * 255


tab = rysuj_ramke(120, 60, 8) 
im_ramka = Image.fromarray(tab)
# im_ramka.show()
# im_ramka.save("obraz1.bmp")

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import random

# Zadanie 1
obraz = Image.open('image.jpg')
oh,ow = obraz.size
# obraz_copy = obraz.copy()
print("tryb obrazu", obraz.mode)
print("rozmiar", obraz.size)

inicjaly = Image.open('inicjaly.bmp')
print("tryb obrazu", inicjaly.mode)
print("rozmiar", inicjaly.size)
ih,iw = inicjaly.size

# # Zadanie 2
# # 2.1
# srodek=lambda i,j: (int(oh/2 - ih/2)+i,int(ow/2 - ih/2)+j)
# prawy_dolny=lambda i,j: (int(oh/2 - ih/2)+i,int(ow/2 - ih/2)+j)
def wstaw_inicjaly(obraz,inicjaly,m,n,kolor):
    obraz_copy = obraz.copy()
    for i in range(ih):
        for j in range(iw):
            if inicjaly.getpixel((i,j)) == 0:
                obraz_copy.putpixel((m+i,n+j),kolor)
    # obraz_copy.show()
    return obraz_copy
obraz1 = wstaw_inicjaly(obraz,inicjaly,400,300,(0,255,0))
obraz1.save("obraz1.jpg")
obraz2 = wstaw_inicjaly(obraz,inicjaly,850,650,(255,0,0))
obraz2.save("obraz2.jpg")

# # 2.2
# def zakres(w, h): # funkcja, która uprości podwójna petle for
#     return [(i, j) for i in range(w) for j in range(h)]

# w miejscu m, n zmienia tylko te pixele, które odpowiadają czarnym pixelom maski, maska jest obrazem czarnobiałym
# kopiuje kwadrat kwadrat o boku 100 z miejsca m,n i wstawia w miejscu m1,n1
def wstaw_inicjaly_maska(obraz, maska, m, n, a, b, c): 
    obraz_copy = obraz.copy()
    for i in range(ih):
        for j in range(iw):
            if maska.getpixel((i, j)) == 0:
                p = obraz_copy.getpixel((i + m, j + n))
                obraz_copy.putpixel((i + m, j + n), (p[0] + a, p[1] + b, p[2] + c))
    # obraz_copy.show()
    return obraz_copy
obraz3 = wstaw_inicjaly_maska(obraz,inicjaly,400,300,-50,255,-50)
obraz3.save("obraz3.jpg")
obraz4 = wstaw_inicjaly_maska(obraz,inicjaly,850,20,255,-50,-50)
obraz4.save("obraz4.jpg")

# Zadanie 3
def wstaw_inicjaly_load(obraz,inicjaly,m,n,kolor):
    obraz_copy = obraz.copy()
    tab_obr = obraz_copy.load()
    tab_inic = inicjaly.load()
    for i in range(ih):
        for j in range(iw):
            if tab_inic[i,j] == 0:
                tab_obr[m+i,n+j] = kolor
    # obraz_copy.show()
    return obraz_copy
wstaw_inicjaly_load(obraz,inicjaly,400,300,(0,255,0))
wstaw_inicjaly_load(obraz,inicjaly,850,650,(255,0,0))

def wstaw_inicjaly_maska_load(obraz, maska, m, n, a, b, c):
    obraz_copy = obraz.copy()
    tab_obr = obraz_copy.load()
    tab_inic = inicjaly.load()
    for i in range(ih):
        for j in range(iw):
            if tab_inic[i, j] == 0:
                p = tab_obr[i + m, j + n]
                tab_obr[i + m, j + n] = (p[0] + a, p[1] + b, p[2] + c)
    # obraz_copy.show()
    return obraz_copy
wstaw_inicjaly_maska_load(obraz,inicjaly,400,300,-50,255,-50)
wstaw_inicjaly_maska_load(obraz,inicjaly,850,20,255,-50,-50)

# Zadanie 4
# 4.1
def konstast(obraz,wsp_kontrastu):
    mn = ((255 + wsp_kontrastu)/255)**2
    img = obraz.point(lambda i: 128+(i-128) * mn)
    # img.show()
    return img
obraz5 = konstast(obraz,100)
obraz5.save("obraz5.jpg")

# 4.2
def negatyw(obraz):
    img = obraz.point(lambda i: 255-i)
    # img.show()
    return img
obraz6 = negatyw(obraz)
obraz6.save("obraz6.jpg")

# 4.3
def transformacja_logarytmiczna(obraz):
    img = obraz.point(lambda i: 255 * np.log(1 + i/255))
    # img.sho()
    return img
obraz7 = transformacja_logarytmiczna(obraz)
obraz7.save("obraz7.jpg")

# 4.4
def transformacja_gamma(obraz,gamma):
    img = obraz.point(lambda i: (i/255) ** (1/gamma)*255)
    # img.show()
    return img
obraz8 = transformacja_gamma(obraz,5)
obraz8.save("obraz8.jpg")

# 4.5
def utnij_wartosci_pikseli(obraz,wsp_min,wsp_max):
    img = obraz.point(lambda i:  random.randint(wsp_min,wsp_max))
    # img.show()
    return img
obraz9 = utnij_wartosci_pikseli(obraz,0,255)
obraz9.save("obraz9.jpg")

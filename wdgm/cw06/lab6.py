from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

# image read function
im = Image.open('image.jpg')
print("tryb obrazu", im.mode)
print("rozmiar", im.size)




def zakres(w, h): # funkcja, która uprości podwójna petle for
    return [(i, j) for i in range(w) for j in range(h)]

# ***********************************************************************************************************************8
# zmiana wartości pikseli za pomocą metod getpixel i putpixel
def pobierz_kolor_pixela(obraz, m, n):  # m, n współrzędne punktu na obrazie
    kolor = obraz.getpixel((m, n))
    return kolor

def wstaw_pixel_w_punkt(obraz, m, n, kolor):  # m, n współrzędne punktu na obrazie, kolor -  dane pixela do wstawienia
    obraz.putpixel((m, n), kolor)
    return obraz

def wstaw_pixel_w_zakresie(obraz, m, n, kolor):  # w miejscu m,n wstawia kwadrat o boku 100 w kolorze "kolor"
    for i, j in zakres(100, 100):
        obraz.putpixel((i + m, j + n), kolor)
    return obraz

def rozjasnij_obraz_putpixel(obraz, a, b, c): # zmienia wartości każdego kanału
    w, h = obraz.size
    for i, j in zakres(w, h):
        p = obraz.getpixel((i, j))
        obraz.putpixel((i, j), (p[0] + a, p[1] + b, p[2] + c))
    return obraz

def rozjasnij_obraz_w_zakresie(obraz, m, n, a, b, c):  # w miejscu m,n "rozjaśnia" kwadrat o boku 100
    for i, j in zakres(100, 100):
        p = obraz.getpixel((i + m, j + n))
        obraz.putpixel((i + m, j + n), (p[0] + a, p[1] + b, p[2] + c))
    return obraz

def skopiuj_obraz_w_zakresie(obraz, m, n, m1, n1):  # kopiuje kwadrat kwadrat o boku 100 z miejsca m,n i wstawia w miejscu m1,n1
    for i, j in zakres(100, 100):
        p = obraz.getpixel((i + m, j + n))
        obraz.putpixel((i + m1, j + n1), p)
    return obraz

def rozjasnij_obraz_z_maska(obraz, maska, m, n, a, b, c):  # w miejscu m, n zmienia tylko te pixele, które odpowiadają czarnym pixelom maski, maska jest obrazem czarnobiałymkopiuje kwadrat kwadrat o boku 100 z miejsca m,n i wstawia w miejscu m1,n1
    w, h = maska.size
    for i, j in zakres(w, h):
        if maska.getpixel((i, j)) == 0:
            p = obraz.getpixel((i + m, j + n))
            obraz.putpixel((i + m, j + n), (p[0] + a, p[1] + b, p[2] + c))
    return obraz

def dodaj_szum(obraz, n, kolor1, kolor2):# dodawanie szumu typu salt and pepper
    w, h = obraz.size
    x, y = np.random.randint(0, w, n), np.random.randint(0, h, n) # powtarza n razy losowanie z zakresu 0,w i z zakresu 0,h
    for (i, j) in zip(x, y):  # zip robi pary z list x,y
        obraz.putpixel((i, j), (kolor1 if np.random.rand() < 0.5 else kolor2))  # salt-and-pepper
    return obraz

im1 = im.copy()
im2 = im.copy()
im3 = im.copy()
im4 = im.copy()
im5 = im.copy()
maska = Image.open('gwiazdka.bmp')
im6 = im.copy()

plt.title("1. wstaw_pixel_w_zakresie(im1, 200, 100, (200, 200, 200))")
plt.axis('off')
plt.imshow(wstaw_pixel_w_zakresie(im1, 200, 100, (200, 200, 200)))
plt.show()

plt.title("2. rozjasnij_obraz_putpixel(im2, 50, 100, -50)")
plt.axis('off')
plt.imshow(rozjasnij_obraz_putpixel(im2, 50, 100, -50))
plt.show()

plt.title("3. rozjasnij_obraz_w_zakresie(im3, 200, 100, 50, 100, -50)")
plt.axis('off')
plt.imshow(rozjasnij_obraz_w_zakresie(im3, 200, 100, 50, 100, -50))
plt.show()

plt.title("4. skopiuj_obraz_w_zakresie(im4, 50, 50, 200, 100)")
plt.axis('off')
plt.imshow(skopiuj_obraz_w_zakresie(im4, 50, 50, 200, 100))
plt.show()

plt.title("5. rozjasnij_obraz_z_maska(im5, maska,  200, 100, 50, 100, -50)")
plt.axis('off')
plt.imshow(rozjasnij_obraz_z_maska(im5, maska,  200, 100, 50, 100, -50))
plt.show()

plt.title("6.  dodaj_szum(im6, 50000, (0,0,0), (255,255,255) ")
plt.axis('off')
plt.imshow(dodaj_szum(im6, 50000, (0,0,0), (255,255,255)))
plt.show()
# ****************************************************************


# zmiana wartości pikseli za pomocą metody  load


# funkcja wykorzystująca inne funkcje na wartościach pikseli
def zastosuj_funkcje(image, func):
    w, h = image.size
    pixele = image.load()
    for i, j in zakres(w, h):
        pixele[i, j] = func(pixele[i, j])

def przestaw_kolory(pixel):
    return (pixel[1], pixel[2], pixel[0])


def rozjasnij(pixel):
    return (pixel[0] + 100, pixel[1] + 100, pixel[2] + 100)

im7 = im.copy()
zastosuj_funkcje(im7, przestaw_kolory)
plt.title("7. Metoda load i funkcja przestaw_kolory  ")
plt.axis('off')
plt.imshow(im7)
plt.show()

im8 = im.copy()
zastosuj_funkcje(im8, rozjasnij)
plt.title("8. Metoda load i funkcja rozjasnij ")
plt.axis('off')
plt.imshow(im8)
plt.show()

# ****************************************************************


# zmiana wartości pikseli za pomocą metody  point
im9 = im.copy()
im9 = im9.point(lambda i: i + 100)
plt.title("9.  Metoda point - rozjasnij ")
plt.axis('off')
plt.imshow(im9)
plt.show()
# ****************************************************************

# obcięcie wartości pixeli do pewnej wartości treshhold
im10 = im.copy()
wsp_obciecia = 180
im10 = im10.point(lambda i: i > wsp_obciecia and 255)
plt.title("10.   Metoda point - Obcięcie ")
plt.axis('off')
plt.imshow(im10)
plt.show()

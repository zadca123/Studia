from PIL import Image  # Python Imaging Library
import numpy as np


# # Zadanie1
# # podpunkt 1.1
def rysuj_ramke(w, h, dzielnik):
    t = (h, w)  # rozmiar tablicy
    tab = np.zeros(t, dtype=np.uint8)  # deklaracja tablicy wypełnionej zerami - czarna
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości  ramki
    z1 = h - grub
    z2 = w - grub
    for i in range(1, int(h / grub)+ 1):
        if i % 2 == 1:
            tab[i * grub:z1 - (i-1) * grub, i * grub:z2 - (i-1) * grub] = 1
        else:
            tab[i * grub:z1 - (i-1) * grub, i * grub:z2 - (i-1) * grub] = 0
    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz1.bmp")
rysuj_ramke(120, 60, 8) 

# podpunkt 1.2
def rysuj_pasy_pionowe(w, h, dzielnik):
    t = (h, w)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    # tab = np.zeros(t, dtype=np.uint8)
    # # jaki bedzie efekt, gdy np.ones zamienimy na np.zeros?
    # W moim przypadku brak efektu..., lecz zapewne zmieniła by się kolejność pasów, rozpoczynałyby się od białego.
    grub = int(w / dzielnik) 
    for k in range(dzielnik):
        for g in range(grub):
            i = k * grub + g 
            for j in range(h):
                tab[j, i] = k % 2  # reszta z dzielenia przez dwa
    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz2.bmp")
    # obraz.show()
rysuj_pasy_pionowe(600,200, 20)

# # podpunkt 1.3
def styk_w_punkcie(m,n):
    w=120
    h=60
    t = (h,w)  # rozmiar tablicy
    tab = np.ones(t, dtype=np.uint8)
    for y in range(w):
        for x in range(h):
            if (x<n and y<m) or (x>n and y>m):
                tab[x,y] = 0
    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz3.bmp")
    # obraz.show()
styk_w_punkcie(10,40)


# # podpunkt 1.4
# Stwarza czarny krzyż na białym tle, można ustawic grubość i wymiary obrazka
def krzyz(w,h, grub):
    t = (h, w)
    tab = np.ones(t,dtype=np.uint8)

    tab[ int(h/2)-int(grub/2):int(h/2)+int(grub/2), :] = 0
    tab[ :, int(w/2)-int(grub/2):int(w/2)+int(grub/2)] = 0

    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz4.bmp")
    # obraz.show()
krzyz(200, 100, 35)

# # podpunkt 1.4
# kratownica
def kratownica(w,h, dzielnik):
    t = (h, w)
    tab = np.ones(t,dtype=np.uint8)
    grub = int(min(w, h) / dzielnik)  # wyznaczenie grubości kratki
    setter = True
    for y in range(0,h,grub):
        for x in range(0,w,grub):
            if y%grub==1:
                if setter:
                    tab[y:, x: ] = 1
                else:
                    tab[y:, x: ] = 0
            else:
                if setter:
                    tab[y:, x: ] = 0
                else:
                    tab[y:, x: ] = 1
            setter = not setter
                
    tab = tab * 255 # bez tego obraz jest cały czarny
    obraz = Image.fromarray(tab)
    obraz.save("zad1_obraz4_1.bmp")
    # obraz.show()
kratownica(200, 100, 10)


# # ZADANIE 2
def negatyw(tab):
    for y in range(len(tab)):
        for x in range(len(tab[0])):
            if tab[y,x] == 0:
                tab[y,x] = 1
            else:
                tab[y,x] = 0
    # tab = tab * 255 # teraz to nie jest potrzebne
    obraz = Image.fromarray(tab)
    obraz.save("inicjaly_negatyw.bmp")
    return tab
obraz = Image.open("inicjaly.bmp")
obraz_dane = np.asarray(obraz)
obraz_negatyw = negatyw(obraz_dane)
# print(obraz_negatyw)


# # # ZADANIE 3
# # 3.1
obraz_trojkat = Image.open("trojkat.bmp") 
obraz_elipsa = Image.open("elipsa.bmp")
obraz_trojkat_dane = np.array(obraz_trojkat, dtype=np.uint8) 
obraz_elipsa_dane = np.array(obraz_elipsa, dtype=np.uint8)

# # # 3.2.1
def wspolna_czarnych(obraz1, obraz2):
    # for y in range(len(obraz1)):
    #     for x in range(len(obraz1[0])):
    #         # if obraz1[y,x]+obraz2[y,x] >= 1:
    #         #     obraz1[y,x] = 1
    obraz1 = obraz1 + obraz2
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_321.bmp")
    # obraz.show()
wspolna_czarnych(obraz_elipsa_dane, obraz_trojkat_dane)

# 3.2.2
def suma_czarnych(obraz1, obraz2):
    # for y in range(len(obraz1)):
    #     for x in range(len(obraz1[0])):
    #         obraz1[y,x] = obraz1[y,x] * obraz2[y,x]
    obraz1 = obraz1 * obraz2
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_322.bmp")
    # obraz.show()
suma_czarnych(obraz_elipsa_dane, obraz_trojkat_dane)

# 3.2.3
def roznica_czarnych(obraz1, obraz2):
    for y in range(len(obraz1)):
        for x in range(len(obraz1[0])):
            if obraz1[y,x] == obraz2[y,x] == 0:
                obraz1[y,x] = 1
    # obraz1 = obraz2-obraz1       
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_323.bmp")
    # obraz.show()
roznica_czarnych(obraz_elipsa_dane, obraz_trojkat_dane)

# # 3.2.4
def wspolna_bialych(obraz1, obraz2):
    # for y in range(len(obraz1)):
    #     for x in range(len(obraz1[0])):
    #         if not obraz2[y,x] == obraz1[y,x]:
    #             obraz1[y,x] = 0
    #         else:
    #             obraz1[y,x] = 1

    obraz1 = negatyw(obraz1 * obraz2)
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_324.bmp")
    # obraz.show()
wspolna_bialych(obraz_elipsa_dane,obraz_trojkat_dane)

# # 3.2.5
def suma_bialych(obraz1, obraz2):
    # for y in range(len(obraz1)):
    #     for x in range(len(obraz1[0])):
    #         if (obraz1[y,x] == 1 and obraz2[y,x] == 1):
    #             obraz1[y,x] = 0
    #         else:
    #             obraz1[y,x] = 1
    #         # if obraz1[y,x] + obraz2[y,x]:
    #         #     obraz1[y,x] == 1
    #         # obraz1[y,x] = obraz1[y,x] + obraz2[y,x]
    obraz1 = negatyw(obraz1 + obraz2)
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_325.bmp")
    # obraz.show()
suma_bialych(obraz_elipsa_dane, obraz_trojkat_dane)

# # 3.2.6
def roznica_bialych(obraz1, obraz2):
    # for y in range(len(obraz1)):
    #     for x in range(len(obraz1[0])):
    #         if obraz1[y,x]==obraz2[y,x]:
    #             obraz1[y,x] = 1
    for y in range(len(obraz1)):
        for x in range(len(obraz1[0])):
            if not obraz2[y,x] == obraz1[y,x]:
                obraz1[y,x] = 1
            else:
                obraz1[y,x] = 0
    obraz1 = obraz1 * 255
    obraz = Image.fromarray(obraz1)
    obraz.save("zad3_326.bmp")
    # obraz.show()
roznica_bialych(obraz_elipsa_dane, obraz_trojkat_dane)




# # edycja tablicy obrazu, pobieranie informacji uzywajac flags
# im1 = Image.open('kolo.bmp')
# # tablica obrazu
# T1 = np.array(im1)
# # wyświelta flagi
# print(T1.flags)

# # blokada edycji
# T1.setflags(write=0)
# print("..........po zablokowaniu edycji....")
# print(T1.flags)

# # metoda uzsykania dostepu do tablicy w przypadku OWNDATA: False, stworzenie kopii
# im1_copy = im1.copy()
# T1 = np.array(im1.copy)
# print("..........wczytywanie do tablicy z kopii obrazu....")
# print(T1.flags)


# # operacje boolowskie
# a = True

# # negacja
# negacja = not a
# print(a, negacja)

# # iloczyn
# b = True
# iloczyn = bool(a * b)
# print(a, b, iloczyn)
# c = False
# iloczyn = bool(a * c)
# print(a, c, iloczyn)

# # suma
# b = True
# a1 = not a
# b1 = not b
# iloczyn1 = a1 * b1
# suma = not iloczyn1
# print(a, b, suma)




# # operacje boolowskie na tablicach
# # iloczyn = częśc wspólna Tab1*Tab2 tzn. iloczyn boolowski odpowiednich wyrazów
# # suma = Tab1 + Tab 2 tzn suma boolowska odpowiednich wyrazów 
# # negacja - trzeba napisac program, który neguje kazdy element tablicy


# im1_tmp = Image.open('kolo.bmp')
# im1 = im1_tmp.copy()
# t1 = np.array(im1)
# im2_tmp = Image.open('gwiazdka.bmp')
# im2 = im2_tmp.copy()
# t2 = np.array(im2)

# iloczyn = t1 * t2
# iloczyn_im = Image.fromarray(iloczyn)
# iloczyn_im.show()

# suma = t1 + t2
# suma_im = Image.fromarray(suma)
# suma_im.show()

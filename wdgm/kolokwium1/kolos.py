import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PIL import ImageChops

# # Zadanie 1
# # obraz = Image.open("obrazy/obraz5.jpg")
# # # print(obraz.getpixel(()))
# # tab = np.array(obraz)
# # print("typ", obraz.mode)
# # print("typ danych tablicy", tab.dtype)  # typ danych przechowywanych w tablicy
# # print("rozmiar tablicy", tab.shape)  # rozmiar tablicy - warto porównac z wymiarami obrazka
# # print("liczba elementow", tab.size)  # liczba elementów tablicy
# # print("wymiar tablicy", tab.ndim) # wymiar mówi czy to jest talica 1D, 2d, 3D ...
# # print("wymiar wyrazu tablicy", tab.itemsize)  # pokazuje ile współrzednych trzeba do opisania wyrazu tablicy (piksela)
# # print("pierwszy wyraz", tab[0][0])
# # print("drugi wyraz", tab[1][0])
# # print("format", obraz.format)
# # print ("rozmiar", obraz.size)
# # print(obraz.format, ", ", tab.dtype, ", ")
# # print(f"{obraz.format}, {tab.dtype}, {obraz.getpixel((379,65))}, {tab[259,546]}")


# # srodek=lambda i,j: (int(oh/2 - ih/2)+i,int(ow/2 - ih/2)+j)
# # prawy_dolny=lambda i,j: (int(oh/2 - ih/2)+i,int(ow/2 - ih/2)+j)
# # img = obraz.point(lambda i: (lambda i,val1,val2: 0 if i <= val1 else 1 if i >= val2 else i)(i,wsp_min,wsp_max))

# # diff = ImageChops.difference(obraz1,obraz2)
# # diff.show()


# # # # zadanie def zad3(tab):
# # im = Image.open('obrazy/obraz9.jpg')
# # maska = Image.open('obrazy/maska2.jpg')
# # tab = np.array(im)
# # # im.show()

# # def zakres(w, h): # funkcja, która uprości podwójna petle for
# #     return [(i, j) for i in range(w) for j in range(h)]

# def wstaw_inicjaly_maska(obraz, inicjaly, m, n, x, y, z): 
#     # w, h = inicjaly.size
#     # obrazcopy = obraz.copy()
#     # pikseleo = obrazcopy.load()
#     # pikselei = inicjaly.load()
#     # for i, j in zakres(w, h):
#     #     if pikselei[i, j] == 0:
#     #         p = pikseleo[i+m,j+n]
#     #         pikseleo[i + m, j + n] = (p[0] + x, p[1] + y, p[2] + z)
#     tab_neg = tab.copy()
#     for i in range(h):
#         for j in range(w):
#             tab_neg[i, j, 0] = tab[i, j, 0] - 150
#             tab_neg[i, j, 1] = tab[i, j, 1] + 140
#             tab_neg[i, j, 2] = tab[i, j, 2] - 110
#    return tab_neg

# # # h, w, kolor = tab.shape
# # def foo(tab):

# # res = wstaw_inicjaly_maska(im, maska, 0, 0, 190, 180, 160).show()
# # res.save("zadanie3.jpg")

# # # # Zadanie 5
# # # txt = np.loadtxt('obrazy/tab2.txt')
# # # im = Image.open('obrazy/obraz7.jpg')
# # # print (txt)
# # img = Image.open('obrazy/obraz7.jpg')
# # text = np.loadtxt('obrazy/tab1.txt', dtype=np.uint8) * 255
# # img_text = Image.fromarray(text)
# # img_text.save('dd.jpg')
# # print(img.size)
# # print(img_text.size)

# # def wstaw_inicjaly(obraz, inicjaly, kolor):
# #     inicjaly_matrix = np.array(inicjaly)
# #     obraz_copy = obraz.copy()
# #     w, h = obraz.size
# #     for i in range(len(inicjaly_matrix)):
# #         for j in range(len(inicjaly_matrix[0])):
# #             if inicjaly_matrix[i][len(inicjaly_matrix[0]) - j - 1] == 0:
# #                 obraz_copy.putpixel((w - j, i), kolor)
# #     return obraz_copy
# # im = wstaw_inicjaly(img, img_text, (255, 255, 0))
# # im.show()
# # im.save('zadanie5.jpg')

# # def wstaw_inicjaly(obraz, inicjaly, kolor):
# #     inicjaly_matrix = np.array(inicjaly)
# #     obraz_copy = obraz.copy()
# #     w, h = obraz.size
# #     for i in range(len(inicjaly_matrix)):
# #         for j in range(len(inicjaly_matrix[0])):
# #             if inicjaly_matrix[i][len(inicjaly_matrix[0]) - j - 1] == 0:
# #                 obraz_copy.putpixel((w - j, i), kolor)
# #     return obraz_copy

# # im = wstaw_inicjaly(img, img_text, (255, 255, 0))
# # im.show()
# # im.save('zadanie5.jpg')


# # # Zadanie 7
# # obraz3 = Image.open("obrazy/obraz7.jpg")
# # def szary():
    
# # images = [img00,img01,img02,img03,img04,img05]
# # figure = plt.figure(figsize=(32, 32))
# # for i in range(len(images)):
# #     plt.subplot(2, 3, i+1)
# #     plt.imshow(images[i])
# # plt.savefig("mix.jpg")

# # zadanie8
# # def suma_czarnych(obraz1, obraz2):

# #     # for y in range(len(obraz1)):
# #     #     for x in range(len(obraz1[0])):
# #     #         obraz1[y,x] = obraz1[y,x] * obraz2[y,x]
# #     obraz1 = obraz1 * obraz2
# #     obraz1 = obraz1 * 255
# #     obraz = Image.fromarray(obraz1)
# #     obraz.save("czesc_wspolna.jpg")
# # suma_czarnych(Image.open("obrazy/owal2.bmp"),Image.open("obrazy/prostokat2.jpg"))


# # # # # 3.2.1
# # obraz_trojkat = Image.open("obrazy/prostokat2.jpg")
# # obraz_elipsa = Image.open("obrazy/owal2.bmp")
# # tab1 = np.array(obraz_trojkat, dtype=np.uint8) 
# # tab2= np.array(obraz_elipsa, dtype=np.uint8)

# # im1 = Image.open("obrazy/prostokat2.jpg")
# # im1 = im1.convert("L")
# # t1 = np.array(im1)
# # im2 = Image.open('obrazy/owal2.bmp')
# # t2 = np.array(im2)
# # h = im1.height
# # w = im1.width


# # iloczyn = t1 - t2
# # iloczyn_im = Image.fromarray(iloczyn)
# # iloczyn_im.show()
# # iloczyn_im.save("wynik.jpg")

# # # zadanie7
# # img = Image.open('obraz6.jpg')
# # text = np.loadtxt('tab2.txt', dtype=np.uint8) * 255
# # img_text = Image.fromarray(text)
# # img_text.save('dd.jpg')
# # print(img.size)
# # (print(img_text.size))


# # def wstaw_inicjaly(obraz, inicjaly, kolor):
# #     inicjaly_matrix = np.array(inicjaly)
# #     obraz_copy = obraz.copy()
# #     w, h = obraz.size
# #     for i in range(len(inicjaly_matrix)):
# #         for j in range(len(inicjaly_matrix[0])):
# #             if inicjaly_matrix[i][j - 1] == 0:
# #                 obraz_copy.putpixel((j, i), kolor)
# #     return obraz_copy


# # im = wstaw_inicjaly(img, img_text, (255, 255, 0))
# # im.show()
# # im.save('zadanie5.jpg')
# # zadanie 3

im = Image.open('obrazy/obraz8.jpg')
maska = Image.open('obrazy/maska1.jpg')
im.show()

def zakres(w, h): # funkcja, która uprości podwójna petle for
    return [(i, j) for i in range(w) for j in range(h)]

def wstaw_inicjaly_maska(obraz, inicjaly, m, n, x, y, z):
    w, h = inicjaly.size
    obrazcopy = obraz.copy()
    pikseleo = obrazcopy.load()
    pikselei = inicjaly.load()
    for i, j in zakres(w, h):
        if pikselei[i, j] == 0:
            p = pikseleo[i+m,j+n]
            pikseleo[i + m, j + n] = (p[0] + x, p[1] + y, p[2] - z)
    return obrazcopy

# wstaw_inicjaly_maska(im, maska, 0, 0, 130, 120, 160).show()
wstaw_inicjaly_maska(im, maska, 0, 0, 190, 180, 160).save("zad3.jpg")

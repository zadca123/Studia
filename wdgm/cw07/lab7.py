from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import random

obraz = Image.open('obraz.jpg')
h, w = obraz.size

# Zadanie 1 TODO
# 1.1 DONE (niepoprawnie...)
def konwertuj_na_szary(obraz, w1, w2, w3):
    r, g, b = obraz.split()  # powstają obrazy
    r_T = np.array(r, dtype=np.uint8)
    g_T = np.array(g, dtype=np.uint8)
    b_T = np.array(b, dtype=np.uint8)
    w1 = 0.3
    w2 = 0.8
    w3 = 0.2
    szary = w1 * r_T + w2 * g_T + w3 * b_T
    img = Image.fromarray(szary)
    # img = img.point(lambda i: int(i))
    img = img.convert("L")
    # img.show()
    return img
# # 1.2 DONE
obraz1 = konwertuj_na_szary(obraz, 0.2, 0.3, 0.5)
obraz1.save('obraz1.jpg')

# # 1.3 TODO
# def idk(obraz, w1, w2, w3):
    # r, g, b = obraz.split()  # powstają obrazy
    # r_T = np.array(r, dtype=np.uint8)
    # g_T = np.array(g, dtype=np.uint8)
    # b_T = np.array(b, dtype=np.uint8)
    # w1 = 0.3
    # w2 = 0.8
    # w3 = 0.2
    # szary = w1 * r_T + w2 * g_T + w3 * b_T
    # img = Image.fromarray(szary)
    # img.show()
    # return img
# print("tryb obrazu szarego", obraz_szary.mode)

# # Zadanie 2 DONE
def szary_na_czarno_bialy(obraz, wsp):
    img = obraz.point(lambda i: (lambda i,wsp: 255 if i<= wsp else 0)(i,wsp))
    # img.show()
    return img
szary_na_czarno_bialy(obraz1, 125)

# Zadanie 3 DONE
# 3.1 DONE
def utnij_wartosci_pikseli(obraz, wsp_min, wsp_max):
    img = obraz.point(lambda i: (lambda i,val1,val2: val1 if i <= val1 else val2 if i >= val2 else i)(i,wsp_min,wsp_max))
    # img.show()
    return img
# 3.2 DONE
obraz2 = utnij_wartosci_pikseli(obraz, 120, 130)
obraz2.save('obraz2.jpg')
# 3.3 DONE
def utnij_wartosci_pikseli2(obraz, wsp_min, wsp_max):
    img = obraz.point(lambda i: (lambda i,val1,val2: 0 if i <= val1 else 1 if i >= val2 else i)(i,wsp_min,wsp_max))
    # img.show()
    return img
# 3.4 DONE
obraz3 = utnij_wartosci_pikseli2(obraz, 120, 130)
obraz3.save('obraz3.jpg')

# Zadanie 4 DONE
# 4.1 DONE
def rysuj_prostokat(obraz,m,n,a,b,kolor):
    img = obraz.copy()
    tab = np.zeros((b,a), dtype=np.uint8)
    grub = 1 
    z1 = b - grub
    z2 = a - grub
    tab[grub:z1,grub:z2] = 1
    img_ramka = Image.fromarray(tab)
    for i in range(a):
        for j in range(b):
            if img_ramka.getpixel((i, j)) == 0:
                img.putpixel((i+m,j+n), (kolor))
    # img.show()
    return img
# 4.2 DONE
obraz4 = rysuj_prostokat(obraz,50,50,300,500,(0,0,0))
obraz4.save('obraz4.jpg')

# 4.3 DONE
def rysuj_kwadrat(obraz,m,n,a,kolor):
    img = obraz.copy()
    tab = np.zeros((a,a), dtype=np.uint8)
    grub = 1 
    z1 = a - grub
    tab[grub:z1,grub:z1] = 1
    img_ramka = Image.fromarray(tab)
    for i in range(a):
        for j in range(a):
            if img_ramka.getpixel((i, j)) == 0:
                img.putpixel((i+m,j+n), (kolor))
    # img.show()
    return img
# 4.4 DONE
obraz5 = rysuj_kwadrat(obraz,10,10,100, 255)
obraz5.save('obraz5.jpg')

# Zadanie 5
# 5.1 DONE
def odbij_w_poziomie(obraz):
    img = obraz.copy()
    tab = img.load()
    for i in range(h):
        for j in range(int(w/2)):
            temp = tab[i,j]
            tab[i, j] = tab[i, w-j-1]
            tab[i, w-j-1] = temp
    # img.show()
    return img
# 5.2 DONE
obraz6 = odbij_w_poziomie(obraz)
obraz6.save('obraz6.jpg')

# 5.3 DONE
def odbij_w_pionie(obraz):
    img = obraz.copy()
    tab = img.load()
    for i in range(int(h/2)):
        for j in range(w):
            temp = tab[i,j]
            tab[i, j] = tab[h-i-1,j]
            tab[h-i-1, j] = temp
    # img.show()
    return img
# 5.4 DONE
obraz7 = odbij_w_pionie(obraz)
obraz7.save('obraz7.jpg')

# 5.5 DONE
def odbij_w_poziomie_przez_srodek(obraz):
    img = obraz.copy()
    tab = img.load()
    for i in range(h):
        for j in range(int(w/2)):
            tab[i, w-j-1] = tab[i,j]
    # img.show()
    return img
# 5.6 DONE
obraz8 = odbij_w_poziomie_przez_srodek(obraz)
obraz8.save('obraz8.jpg')

# # 5.7 DONE
def odbij_w_pionie_przez_srodek(obraz):
    img = obraz.copy()
    tab = img.load()
    for i in range(int(h/2)):
        for j in range(w):
            tab[h-i-1, j] = tab[i,j]
    # img.show()
    return img
# 5.8 DONE
obraz9 = odbij_w_pionie_przez_srodek(obraz)
obraz9.save('obraz9.jpg')

# Zadanie 6 DONE
# Dzieję się to ponieważ gdy zwiększamy wartości o sto, kanały co już posiadają wysokie wartości przy podaniu liczby sto, zaczynają liczyć od nowa po przekroczeniu wartości 255, resetują się
def foo(obraz):
    # T = np.array(obraz, dtype='uint8')
    # T = 100
    # obraz_wynik = Image.fromarray(T, "RGB")
    # obraz_wynik.show()

    # poprawiony (napisany od nowa...) kod
    # kod dodaje wartość 100 jeżeli suma wartości kanału z wartością 100 jest mniejsza równa 255, inaczej wartość kanału otrzymuje wartość 255
    img = obraz.point(lambda i: i+100 if i+100<=255 else 255)
    # img.show()
foo(obraz)

from PIL import Image
import numpy as np
from PIL import ImageChops, ImageOps, ImageShow, ImageFilter
from PIL import ImageStat as stat
import matplotlib.pyplot as plt
from PIL.ImageFilter import (
    CONTOUR, DETAIL, SHARPEN)

def statystyki(im):
    s = stat.Stat(im)
    print("extrema ", s.extrema)  # max i min
    print("count ", s.count)  # zlicza
    print("mean ", s.mean)  # srednia
    print("median ", s.median)  # mediana
    print("stddev ", s.stddev)  # odchylenie standardowe

# Zadanie 1
obraz = Image.open("image.png")
print(obraz.mode)
# obraz.show()
obraz = obraz.convert("L")
print(obraz.mode)
# obraz_L.show()

# Zadanie 2
statystyki(obraz)
hist = obraz.histogram()
plt.title("histogram - zad2")
plt.plot(hist)
plt.savefig('zad2_hist.jpg', bbox_inches='tight', dpi=150)
plt.show()


# # Zadanie 3
def histogram_norm(obraz):
    count=stat.Stat(obraz).count[0]
    hist = obraz.histogram()
    for i in range(len(hist)):
        hist[i] /= count
    plt.title("histogram - zad3")
    plt.plot(hist)
    plt.savefig('norm.jpg', bbox_inches='tight', dpi=150)
    plt.show()
    return hist
histogram_norm(obraz)

# # Zadanie 4
def histogram_cumul(obraz):
    hist = histogram_norm(obraz)
    prev = 0
    for i in range(len(hist)):
        hist[i] += prev
        prev = hist[i]
    plt.title("histogram - zad4")
    plt.plot(hist)
    plt.savefig('kumul.jpg', bbox_inches='tight', dpi=150)
    plt.show()
    return hist
    # # img = obraz.point(lambda i: (lambda i,val: i/val)(i,stat.Stat(obraz).count[0]))
    # count=stat.Stat(obraz).count[0]
    # img = obraz.point(lambda i: i/val)

    # return hist
histogram_cumul(obraz)

# Zadanie 5
def histogram_equalization(obraz):
    hist = histogram_cumul(obraz)
    img = obraz.point(lambda i: int(255*hist[i]))
    img.save("equalized.jpg")
    img.show()
    return img
obraz_equalized = histogram_equalization(obraz)

# Zadanie 6
obraz_equalized1 = ImageOps.equalize(obraz, mask=None)
obraz_equalized1.save("equalized1.jpg")
# obraz_equalized.show()

# 6.1
diff = ImageChops.difference(obraz_equalized, obraz_equalized1)
diff.save("diff.jpg")
# diff.show()

# 6.2
plt.title("histogram - zad6.2")
plt.bar(range(256), obraz_equalized.histogram(), color='r', alpha=0.5)
plt.bar(range(256), obraz_equalized1.histogram(), color='b', alpha=0.5)
plt.savefig('zad6_2.jpg', bbox_inches='tight', dpi=150)
plt.show()  # obrazy są praktycznie takie same

# Zadanie 7.3
statystyki(obraz_equalized)
# extrema  [(2, 255)]
# count  [41040]
# mean  [127.80631091617934]
# median  [127]
# stddev  [73.48236374675058]

statystyki(obraz_equalized1)
# extrema  [(0, 255)]          # niewielka róznica
# count  [41040]               # to samo
# mean  [127.35402046783626]   # niewielka róznica
# median  [127]                # to samo
# stddev  [74.15058591629993]  # niewielka róznica

# Zadanie 8
obraz1 = obraz.filter(ImageFilter.DETAIL)
obraz2 = obraz.filter(ImageFilter.SHARPEN)
obraz3 = obraz.filter(ImageFilter.CONTOUR)

plt.figure(figsize=(16, 16))
plt.subplot(2,2,1) # ile obrazów w pionie, ile w poziomie, numer obrazu
plt.imshow(obraz1, 'gray')
plt.axis('off')
plt.subplot(2,2,2)
plt.imshow(obraz2, 'gray')
plt.axis('off')
plt.subplot(2,2,3)
plt.imshow(obraz3, 'gray')
plt.axis('off')
plt.subplot(2,2,4)
plt.imshow(obraz_equalized1, 'gray')
plt.axis('off')
plt.subplots_adjust(wspace=0.05, hspace=0.05)
plt.savefig('filtry.jpg', bbox_inches='tight', dpi=150)

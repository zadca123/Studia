from PIL import Image
import numpy as np
from PIL import ImageChops
import matplotlib.pyplot as plt

# Zadanie 1
obraz1 = Image.open("obraz1.png").convert("RGB")
obraz2 = Image.open("obraz2.jpg").convert("RGB")
obraz3 = Image.open("obraz3.png")
obraz4 = Image.open("obraz4.jpeg")
obraz5 = Image.open("obraz5.jpeg")
obraz6 = Image.open("obraz6.jpeg")
mask1 = Image.open("mask1.jpg").convert("1")
mask2 = Image.open("mask2.jpg").convert("L")
mask3 = Image.open("mask3.png").convert("RGBA")
print(f"obraz1 typ: {obraz1.mode}")
print(f"obraz2 typ: {obraz2.mode}")
print(f"obraz3 typ: {obraz3.mode}")
print(f"obraz4 typ: {obraz4.mode}")
print(f"obraz5 typ: {obraz5.mode}")
print(f"obraz6 typ: {obraz6.mode}")
print(f"mask1 typ: {mask1.mode}")
print(f"mask2 typ: {mask2.mode}")
print(f"mask3 typ: {mask3.mode}")

# Zadanie 2
w1, h1 = obraz1.size
w2, h2 = obraz2.size
W = min(w1, w2)
H = min(h1, h2)
obraz1_res = obraz1.resize((W,H),0)
obraz2_res = obraz2.resize((W,H),0)
mask1_res = mask1.resize((W,H),0)
mask2_res = mask2.resize((W,H),0)
mask3_res = mask3.resize((W,H),0)
img1_cropped = obraz1_res.crop((10, 10, 300, 350))
img2_cropped = obraz2_res.crop((10, 10, 300, 350))
mask1_img1 = mask1_res.resize(img1_cropped.size, 0)
mask2_img1 = mask2_res.resize(img1_cropped.size, 0)
mask3_img1 = mask3_res.resize(img1_cropped.size, 0)
mask1_img2 = mask1_res.resize(img2_cropped.size, 0)
mask2_img2 = mask2_res.resize(img2_cropped.size, 0)
mask3_img2 = mask3_res.resize(img2_cropped.size, 0)
img1_1 = obraz1_res.copy()
img1_2 = obraz1_res.copy()
img1_3 = obraz1_res.copy()
img2_1 = obraz2_res.copy()
img2_2 = obraz2_res.copy()
img2_3 = obraz2_res.copy()
img1_1.paste(img2_cropped, (10, 10), mask1_img2) 
img1_2.paste(img2_cropped, (10, 10), mask2_img2) 
img1_3.paste(img2_cropped, (10, 10), mask3_img2) 
img2_1.paste(img1_cropped, (10, 10), mask1_img1) 
img2_2.paste(img1_cropped, (10, 10), mask2_img1) 
img2_3.paste(img1_cropped, (10, 10), mask3_img1)

images = [img1_1, img1_2, img1_3, img2_1, img2_2, img2_3]
figure = plt.figure(figsize=(32, 32))
for i in range(len(images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(images[i])
plt.savefig("zadanie2.png")

# Zadanie 3
img1_2 = Image.composite(obraz1_res,obraz2_res, mask2_res)
img1_3 = Image.composite(obraz1_res,obraz2_res, mask3_res)
img2_2 = Image.composite(obraz2_res,obraz1_res, mask2_res)
img2_3 = Image.composite(obraz2_res,obraz1_res, mask3_res)

images = [img1_2, img1_3,img2_2, img2_3]
figure = plt.figure(figsize=(32, 32))
for i in range(len(images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(images[i])
plt.savefig("zadanie3.png")

# Zadanie 4
r,g,b,a = obraz3.split()
img = obraz4.copy()
img.paste(obraz3, (700, 400),a)
img.save("zadanie4.png")
# tlo.show()

# Zadanie 5
w1,h1 = obraz5.size
w2,h2 = obraz6.size
W = min(w1,w2)
H = min(h1,h2)
im_s = obraz5.resize((W, H), 0)  # Image.NEAREST (0)
im_o = obraz6.resize((W, H), 0)
i = 1
plt.figure(figsize=(18,15))
for alpha in np.linspace(0,1,20):
    plt.subplot(4,5,i)
    mix = Image.blend(im_o, im_s, alpha=alpha)
    plt.imshow(mix)
    plt.axis('off')
    i += 1
plt.subplots_adjust(wspace=0.05, hspace=0.05)
plt.savefig("zadanie5.png")

# Zadanie 6
img = obraz1.copy()
w,h=img.size
right = img.crop((int(w/2), 0,w,h))
right = right.transpose(Image.FLIP_LEFT_RIGHT)
kok = img.copy()
kok.paste(right,(0,0))
left = img.crop((0, 0, int(w/2), h))
tot = img.transpose(Image.FLIP_LEFT_RIGHT)
tot.paste(left, (0,0))
kto_wygra = Image.new("RGB", (w*2,h), (255,255,255))
kto_wygra.paste(kok,(0,0))
kto_wygra.paste(tot,(w,0))
kto_wygra.save("zadanie6.png")
# kto_wygra.show()
# obraz powstał po złączeniu ze sobą dwóch obrazów które są odbiciami lustrzanymi

# Zadanie 7
w,h = obraz3.size
small = min(w,h)-20
glowa = obraz3.resize(size=(small,small), box=(50,10, 150, 110))
r,g,b,a = glowa.split()
img = obraz3.copy()
img.paste(glowa, (5, 5),a)
img.save("zadanie7.png")
# img.show()

# Zadanie 8
def roll_axis(image, aa,bb): #przewija obraz z lewej na prawo
    xx, yy = image.size
    aa = aa % xx
    bb = bb % yy
    if aa == 0 and bb == 0: return image
    part1 = image.crop((0, 0, aa, bb))
    part2 = image.crop((aa, 0, xx, bb))
    part3 = image.crop((0,bb, aa, yy))
    part4 = image.crop((aa, bb, xx, yy))
    image.paste(part1, (xx-aa, yy-bb, xx, yy))
    image.paste(part2, (0, yy-bb, xx-aa, yy))
    image.paste(part3, (xx-aa, 0, xx, yy-bb))
    image.paste(part4, (0, 0, xx-aa, yy-bb))
    return image
# roll_axis(obraz1, 100,200).show()

# img = Image.composite(obraz2_res, obraz1_res, mask3_res)
# img.show()
# help(Image.composite)

from PIL import Image
import numpy as np
from PIL import ImageChops
import matplotlib.pyplot as plt

# LA kanał alfa dodany do obrazu L, RGBA kanał alfa dodany do obrazu RGB

shrekfiona = Image.open("Shrek_Fiona.png")
print("tryb shrek i fiona", shrekfiona.mode)
# shrekfiona.show()
sf0 = shrekfiona.convert('RGB') # brzydko  działa konwersja
print("tryb shrek i fiona po konwersji", sf0.mode)
# sf0.show()
r,g,b,a = shrekfiona.split()
# a.show()
sf1 = Image.merge('RGB', (r,g,b)) # ten sam efekt co convert('RGB')
# sf1.show()
print("tryb shrek i fiona merge", sf1.mode)
tlo = Image.new('RGB', shrekfiona.size, (255,255,255)) # nowy obraz wypełniony na biało
tlo.paste(shrekfiona, (0, 0), a)
print('tryb tło', tlo.mode)
# tlo.show()
# input()

# kolejny przykład
shrek = Image.open("Shrek.png")
motyl = Image.open('motyl.png')

print("typ shrek", shrek.mode)
# plt.title("obraz oryginalny ")
# plt.axis('off')
# plt.imshow(shrek)
# plt.show()


shrek0 = shrek.convert('RGB')
print("typ shrek po konwersji", shrek0.mode)
# plt.title("konwersja 'RGB ")
# plt.axis('off')
# plt.imshow(shrek0)
# plt.show()

r, g, b, a = shrek.split()
# a.show()

shrek1 = Image.merge("RGB", (r, g, b))
# plt.title("merge kanałow 'RGB ")
# plt.axis('off')
# plt.imshow(shrek1)
# plt.show()

shrek1.putalpha(a)
# shrek1.show()

print("typ motyl", motyl.mode)
# plt.title("obraz oryginalny ")
# plt.axis('off')
# plt.imshow(motyl)
# plt.show()


motyl0 = motyl.convert('RGB')
print("typ motyl po konwersji", motyl0.mode)
# plt.title("konwersja 'RGB ")
# plt.axis('off')
# plt.imshow(motyl0)
# plt.show()

r, g, b, a = motyl.split()
# a.show()

motyl1 = Image.merge("RGB", (r, g, b))
# plt.title("merge kanałow 'RGB ")
# plt.axis('off')
# plt.imshow(motyl1)
# plt.show()


#  metoda  paste bez maski i z maska

tlo = Image.open('tlo.png')

print(shrek.size, tlo.size)
tlo = tlo.resize(shrek.size, 1)
print(shrek.size, tlo.size)
print(shrek.mode)
r, g, b, a = shrek.split()

# mask = Image.merge("L", (a,))
tlo0 = tlo.copy()
shrek2 = shrek.copy()
tlo0.paste(shrek2, (0, 0), a)  # shrek2 wklejony w tlo0 z maska a

print("typ shrek2", shrek2.mode)
# plt.title("Shrek na tle ")
# plt.axis('off')
# plt.imshow(tlo0)
# plt.show()

img4 = shrek.copy()
img4.paste(tlo, (0, 0), a) #  tlo0 wklejone w shrek2  z maska a
print("typ img", img4.mode)
# plt.title("Shrek wycięty w tle ")
# plt.axis('off')
# plt.imshow(img4)
# plt.show()

motyl2 = motyl.resize(shrek.size)
img5 = Image.alpha_composite(motyl2, shrek)  # kompozycja motyl2 i shrek -- ważna jest kolejnośc argumentów
# plt.title("Shrek na motylu ")
# plt.axis('off')
# plt.imshow(img5)
# plt.show()

img6 = Image.alpha_composite(shrek, motyl2)
# plt.title("Motyl na Shreku")
# plt.axis('off')
# plt.imshow(img6)
# plt.show()

# ********************************************************************************
im = Image.open('szympans.jpg')
im1 = Image.open('ona.jpg')
maska = Image.open('owal.bmp')
# przygotowanie masek
maska_rob = Image.open('gwiazdki.png')
print("tryb maski maska_rob ", maska_rob.mode)
maska_g = maska_rob.convert("1")
print("tryb maski maska_g", maska_g.mode) # maska w trybie 1
print("tryb maski maska", maska.mode) # maska w trybie 1

# przygotowanie rozmiarów obrazów i masek
w, h = im.size
w1, h1 = im1.size

print("szympans", w, h)
print("ona", w1, h1)
W = min(w, w1)
H = min(h, h1)
print(W, H)

im_s = im.resize((W, H), 0)
im_o = im1.resize((W, H), 0)
maska_g = maska_g.resize((W, H), 0)

im2 = im_s.copy()
im_o1 = im_o.crop((10, 10, 300, 350)) # wycięty fragment obrazu im_o
maska_g1 = maska_g.resize(im_o1.size, 0)
# maska_g1.show()

im2.paste(im_o1, (10, 10), maska_g1) # wycięty fragment wstawiony w obraz im2 w miejscu (10, 10) z maską
# im2.show()
# plt.title("crop i paste z maską")
# plt.axis('off')
# plt.imshow(im2)
# plt.show()

im3 = Image.composite(im_s, im_o, maska) # kompozycja z maską
# plt.title("composite")
# plt.axis('off')
# plt.imshow(im3)
# plt.show()

im4 = im_o.copy()
im4.paste(im_s, (0,0), maska)
# plt.title("równoważne composite")
# plt.axis('off')
# plt.imshow(im4)
# plt.show()

motyl1 = motyl.resize(im_o.size, 0)
im5 = Image.composite(im_s, im_o, motyl1)
plt.title("z maska RGBA")
plt.axis('off')
plt.imshow(im5)
plt.show()

# metoda blend -----mieszanie obrazów


mix = Image.blend(im_s, im_o, alpha = 0.3) # im_s * (1- alpha ) + im_o * alpha
# mix.show()


i = 1
plt.figure(figsize=(18,15))
for alpha in np.linspace(0,1,20):
    plt.subplot(4,5,i)
    # mix = Image.blend(resized_im_o, resized_im_s, alpha= alpha)
    mix = Image.blend(im_o, im_s, alpha=alpha)
    plt.imshow(mix)
    plt.axis('off')
    i += 1
plt.subplots_adjust(wspace=0.05, hspace=0.05)
# plt.show()

##### drugi przykład    *********************************************************************

im_M = Image.open('messi.jpg')
im_R = Image.open('Ronaldo.jpg')

# im_M.show()
# im_R.show()
im_R1 = im_R.transpose(Image.FLIP_LEFT_RIGHT) # zastosowalam FLIP, żeby Ronaldo patzrył w tę samą strone co Messi
# im_R1.show()


im1 = im_M.copy()
im2 = im_R1.copy()
w1, h1 = im1.size
w2, h2 = im2.size

print("Messi", w1, h1)
print("Ronaldo", w2, h2)
W = min(w1, w2)
H = min(h1, h2)
print(W, H)

im_s = im1.resize((W, H), 0)  # Image.NEAREST (0)
im_o = im2.resize((W, H), 0)

i = 1
plt.figure(figsize=(18,15))
for alpha in np.linspace(0,1,20):
    plt.subplot(4,5,i)
    mix = Image.blend(im_o, im_s, alpha=alpha)
    plt.imshow(mix)
    plt.axis('off')
    i += 1
plt.subplots_adjust(wspace=0.05, hspace=0.05)
plt.show()


##----Zastosowane paste i crop  -------------------------------


def roll(image, delta): #przewija obraz z lewej na prawo
    xsize, ysize = image.size
    delta = delta % xsize
    if delta == 0: return image
    part1 = image.crop((0, 0, delta, ysize))
    part2 = image.crop((delta, 0, xsize, ysize))
    image.paste(part1, (xsize - delta, 0, xsize, ysize))
    image.paste(part2, (0, 0, xsize - delta, ysize))
    return image

im_oc = im_o.copy()
im_roll = roll(im_oc, 100)
im_o.show()
im_roll.show()

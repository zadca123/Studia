from PIL import Image, ImageOps
import numpy as np
from PIL import ImageChops
import matplotlib.pyplot as plt


# Zadanie 1
im = Image.open('image.jpg')
print("typ", im.mode)
print("format", im.format)
print("rozmiar", im.size)
h, w = im.size

# Zadanie 2
# tablica obrazu
T = np.array(im)
#tablica kanału r
t_r = T[:, :, 0]
im_r = Image.fromarray(t_r) # obraz w odcieniuach szarości kanału r
print("typ", im_r.mode)
#tablica kanału g
t_g = T[:, :, 1]
im_g = Image.fromarray(t_g) # obraz w odcieniuach szarości kanału g
print("typ", im_g.mode)
#tablica kanału b
t_b = T[:, :, 2]
im_b = Image.fromarray(t_b) # obraz w odcieniuach szarości kanału b
print ("typ", im_b.mode)

im_r.save("im_r.jpg")
im_g.save("im_g.jpg")
im_b.save("im_b.jpg")

# Zadanie 3
# Kanały pobrane jako obrazy
r, g, b = im.split()  # powstają obrazy
print("image.split(), typ r: ", r.mode)
print("image.split(), typ g: ", g.mode)
print("image.split(), typ b: ", b.mode)
# diff_r = ImageChops.difference(r, im_r)
# diff_g = ImageChops.difference(g, im_g)
# diff_b = ImageChops.difference(b, im_b)

# Zadanie 4
im1 = Image.merge('RGB', (im_r, g, b))  # zamień r na im_r i sprawdź efekt
# im1.show()
# # nie widać większych zmian po zamienieniu r z im_r
# # 4.1
diff_im = ImageChops.difference(im1,im)
# diff_im.show()

# 4.2
T1 = np.array(im1)
porownanie = T == T1
czy_rowne = porownanie.all()
print (czy_rowne)

# Zadanie 5
t = (w, h)
B = np.ones(t, dtype=np.uint8)
for i in range(w):
    for j in range(h):
        B[i, j] = (i + j) % 256
B_im = Image.fromarray(B)
B_im.save("zad5.jpg")
# B_im.show()

scalony1 = Image.merge('RGB', (B_im, im_r,  g))
scalony2 = Image.merge('RGB', (B_im, r,     im_b))
scalony3 = Image.merge('RGB', (B_im, im_b,  r))
scalony4 = Image.merge('RGB', (B_im, b,     im_g))
scalony5 = Image.merge('RGB', (B_im, im_g,  r))
scalony6 = Image.merge('RGB', (B_im, g,     im_b))

plt.figure(figsize=(32, 16))
plt.subplot(2,3,1) # ile obrazów w pionie, ile w poziomie, numer obrazu
plt.imshow(scalony1)
plt.axis('off')
plt.subplot(2,3,2)
plt.imshow(scalony2)
plt.axis('off')
plt.subplot(2,3,3)
plt.imshow(scalony3)
plt.axis('off')
plt.subplot(2,3,4)
plt.imshow(scalony4)
plt.axis('off')
plt.subplot(2,3,5)
plt.imshow(scalony5)
plt.axis('off')
plt.subplot(2,3,6)
plt.imshow(scalony6)
plt.axis('off')
plt.subplots_adjust(wspace=0.05, hspace=0.05)
# plt.show()

# Zadanie 6
r_T = np.array(r)
g_T = np.array(g)
b_T = np.array(b)
w1 = 0.2
w2 = 0.6
w3 = 0.3
szary = w1 * r_T + w2 * g_T + w3 * b_T
szary_im = Image.fromarray(szary)
szary_im = szary_im.convert("L")
szary_im.save("zad6.jpg")
# szary_im.show()

# # Zadanie 7
obraz1 = Image.open("kolo.bmp")
obraz2 = Image.open("trojkat.bmp")
obraz3 = Image.open("kwadrat.bmp")
obraz1 = obraz1.convert("L")
obraz2 = obraz2.convert("L")
obraz3 = obraz3.convert("L")

scalony1 = Image.merge('RGB', (obraz1, obraz2, obraz3))
scalony2 = Image.merge('RGB', (obraz1, obraz3, obraz2))
scalony3 = Image.merge('RGB', (obraz2, obraz1, obraz3))
scalony4 = Image.merge('RGB', (obraz2, obraz3, obraz1))
scalony5 = Image.merge('RGB', (obraz3, obraz1, obraz2))
scalony6 = Image.merge('RGB', (obraz3, obraz2, obraz1))

plt.figure(figsize=(32, 16))
plt.subplot(3,2,1) # ile obrazów w pionie, ile w poziomie, numer obrazu
plt.imshow(scalony1)
plt.axis('off')
plt.subplot(3,2,2)
plt.imshow(scalony2)
plt.axis('off')
plt.subplot(3,2,3)
plt.imshow(scalony3)
plt.axis('off')
plt.subplot(3,2,4)
plt.imshow(scalony4)
plt.axis('off')
plt.subplot(3,2,5)
plt.imshow(scalony5)
plt.axis('off')
plt.subplot(3,2,6)
plt.imshow(scalony6)
plt.axis('off')
plt.subplots_adjust(wspace=0.05, hspace=0.05)
# plt.show()

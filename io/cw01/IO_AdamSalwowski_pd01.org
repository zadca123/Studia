#+TITLE: Praca domowa nr. 1 z inżynierii oprogramowania 
#+AUTHOR: Adam Salwowski
#+LATEX_HEADER: \usepackage[top=100pt,bottom=100pt,left=75pt,right=75pt]{geometry}

* Pytania
Strona poddana analizie: https://alternativeto.net/
** Dla kogo jest ten SI ? – tzn. przewidywani użytkownicy danego SI,
   Ten system informatyczny przeznaczony jest dla osób szukających alternatywy dla obecnego programu który nie sprostaje ich oczekiwaniom.
** Czy jest dopasowany do potencjalnych użytkowników – tzn. czy będą oni umieli z niego skorzystać
   Jest dopasowany dla średnio-zaawansowanych pasjonatów oprogramowania komputerowego oraz osób szukających nowych rozwiązań w innych programach, zapewne w przedziale wiekowym 20-40 lat.
** Jakie funkcjonalności zawiera?
   Zawiera: 
   - wyszukiwarkę
   - filtry
   - załóż konto
   - zaloguj
   - napisz komentarz (po zalogowaniu)
   - oceń oprogramowanie (po zalogowaniu)
** Jakie zadania pozwala dany SI rozwiązać – tzn. co użytkownik może za pomocą tego SI zrealizować?
   Dzięki temu SI, użytkownik może wyszukać alternatywne oprogramowanie poprzez wpisanie nazwy programu do jakiego szukamy zamiennika, , przeczytać opis programu, komentarze innych użytkowników, dowiedzieć się czy aplikacja nadal jest wspierana przez deweloperów, czy był wyciek baz danych, możliwość sprawdzenia czy oprogramowanie jest płatne, typ licencji dystrybułowania, jakie systemy operacyjne wspierają produkt.
** Czy wszystkie słowa, pojęcia użyte w interfejsie są zrozumiałe dla przewidywanych użytkowników?
   Tak.
** Czy zawiera ułatwienia dla osób z niepełnosprawnościami?
   Nie. Mała podstawowa czcionka, małe przyiski, poruszanie się po stronie wyłącznie klawiaturą lub myszką może sprawiać problemy.
** Czy nawigacja między podstronami jest oczywista?
   Strona domowa nie jest oczywista w nawigacji. Po wyszukaniu programu, nawigacja staje się prosta.
** Czy wyszukiwanie informacji poprzez menu, a nie opcję „wyszukaj”, jest łatwe?
   Bez opuszczania strony domowej poprzez opcję "wyszukaj" nawigacja może sprawiać problemy.
** Po co zrobiono ten SI – tzn. jakie cele poprzez jego utworzenie osiągnięto?
   Ułatwienie procesu szukania aplikacji posiadające podobną funkcjonalność programu do którego szukamy alternatywy.
   
